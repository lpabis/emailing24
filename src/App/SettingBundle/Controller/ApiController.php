<?php

namespace App\SettingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\SettingBundle\Form\Type\ApiType;
use App\SettingBundle\Service\SettingManager;
use App\SettingBundle\Model\SettingsTypeInterface;

class ApiController extends Controller
{
    /**
     * @Route("/api/edit")
     * @Method({"GET"})
     * @Template("AppSettingBundle:Api:editForm.html.twig")
     *
     * @return array
     */
    public function editFormAction()
    {
        $form = $this->get('form.factory')->createNamed('', ApiType::class, $this->getSettingManager()->getSettings(SettingsTypeInterface::TYPE_API));

        return [
            'form' => $form,
        ];
    }

    /**
     * @Route("/api/edit")
     * @Method({"POST"})
     * @Template("AppSettingBundle:Api:editForm.html.twig")
     *
     * @param Request      $request
     *
     * @return array
     */
    public function updateAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed('', ApiType::class, $this->getSettingManager()->getSettings(SettingsTypeInterface::TYPE_API));

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->getSettingManager()->saveSettings($form->getData());

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_setting.api.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_setting_api_editform'));
        }

        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_setting.api.message.updated.error'));

        return [
            'form' => $form
        ];
    }

    /**
     * @return SettingManager
     */
    protected function getSettingManager()
    {
        return $this->get('app_setting.setting.manager');
    }
}