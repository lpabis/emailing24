<?php

namespace App\SettingBundle\Service;

use Doctrine\ORM\EntityManager;
use App\SettingBundle\Entity\Setting;
use App\SettingBundle\Model\AbstractSettings;
use App\SettingBundle\Repository\SettingRepository;

class SettingManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    /**
     * @param EntityManager     $entityManager
     * @param SettingRepository $settingRepository
     */
    public function __construct(EntityManager $entityManager,
                                SettingRepository $settingRepository)
    {
        $this->entityManager = $entityManager;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @param string $groupName
     *
     * @return AbstractSettings
     */
    public function getSettings($groupName)
    {
        $dbSettings = $this->settingRepository->findAll();

        $className = "App\\SettingBundle\\Model\\".ucfirst($groupName).'Settings';

        $settings = new $className();

        foreach($dbSettings as $dbSetting) {
            $settings->setValue($dbSetting->getName(), $dbSetting->getValue());
        }



        return $settings;
    }

    /**
     * @param AbstractSettings $setting
     */
    public function saveSettings(AbstractSettings $setting)
    {
        foreach($setting->getNames() as $methodName) {
            $dbSetting = $this->settingRepository->findOneBy(['name' => $methodName]);
            if(null === $dbSetting) {
                $dbSetting = new Setting();
                $dbSetting->setName($methodName);
            }
            $dbSetting->setValue($setting->getValue($dbSetting->getName()));
            $this->saveSetting($dbSetting);
        }
    }

    public function saveSetting(Setting $setting)
    {
        $this->entityManager->persist($setting);
        $this->entityManager->flush();
    }
}