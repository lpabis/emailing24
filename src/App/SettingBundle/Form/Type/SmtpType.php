<?php

namespace App\SettingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\SettingBundle\Model\SmtpSettings;

class SmtpType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mailerHost', TextType::class,[
                'label' => 'Host',
                'required' => true,
            ])
            ->add('mailerPort', TextType::class,[
                'label' => 'Port',
                'required' => true,
            ])
            ->add('mailerUsername', TextType::class,[
                'label' => 'Login',
                'required' => true,
            ])
            ->add('mailerPassword', PasswordType::class,[
                'label' => 'Hasło',
                'required' => true,
            ]);;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SmtpSettings::class,
            'csrf_protection' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_setting_smtp';
    }
}
