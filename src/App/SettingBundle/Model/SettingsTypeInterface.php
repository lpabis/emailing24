<?php

namespace App\SettingBundle\Model;

interface SettingsTypeInterface
{
    const TYPE_SMTP = 'smtp';
    const TYPE_API = 'api';
}
