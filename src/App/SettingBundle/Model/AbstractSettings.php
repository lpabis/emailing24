<?php

namespace App\SettingBundle\Model;

abstract class AbstractSettings
{
    /**
     * @param string $name
     *
     * @return string|null
     */
    public function getValue($name)
    {
        $method = 'get'.ucfirst($name);

        if(!$this->existMethod($method)) {
            return;
        }

        return $this->$method();
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setValue($name, $value)
    {
        $method = 'set'.ucfirst($name);

        if(!$this->existMethod($method)) {
            return;
        }

        $this->$method($value);
    }

    /**
     * @param string $method
     *
     * @return bool
     */
    protected  function existMethod($method)
    {
        $names = $this->getNames();

        foreach($names as $name) {
            if( $method === 'set'.ucfirst($name) || $method === 'get'.ucfirst($name)) {
                return true;
            }
        }

        return false;

    }

    /**
     * @return string[]|array
     */
    public abstract function getNames();
}