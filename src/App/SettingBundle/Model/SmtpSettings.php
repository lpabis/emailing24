<?php

namespace App\SettingBundle\Model;

class SmtpSettings extends AbstractSettings
{
    /**
     * @var string
     */
    protected $mailerHost;

    /**
     * @var string
     */
    protected $mailerPort;

    /**
     * @var string
     */
    protected $mailerUsername;

    /**
     * @var string
     */
    protected $mailerPassword;

    /**
     * @return string
     */
    public function getMailerHost()
    {
        return $this->mailerHost;
    }

    /**
     * @param string $mailerHost
     */
    public function setMailerHost($mailerHost)
    {
        $this->mailerHost = $mailerHost;
    }

    /**
     * @return string
     */
    public function getMailerPort()
    {
        return $this->mailerPort;
    }

    /**
     * @param string $mailerPort
     */
    public function setMailerPort($mailerPort)
    {
        $this->mailerPort = $mailerPort;
    }

    /**
     * @return string
     */
    public function getMailerUsername()
    {
        return $this->mailerUsername;
    }

    /**
     * @param string $mailerUsername
     */
    public function setMailerUsername($mailerUsername)
    {
        $this->mailerUsername = $mailerUsername;
    }

    /**
     * @return string
     */
    public function getMailerPassword()
    {
        return $this->mailerPassword;
    }

    /**
     * @param string $mailerPassword
     */
    public function setMailerPassword($mailerPassword)
    {
        $this->mailerPassword = $mailerPassword;
    }

    /**
     * @return string[]|array
     */
    public function getNames()
    {
        return ['mailerHost', 'mailerPort', 'mailerUsername', 'mailerPassword'];
    }
}