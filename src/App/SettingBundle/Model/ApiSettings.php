<?php

namespace App\SettingBundle\Model;

class ApiSettings extends AbstractSettings
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string[]|array
     */
    public function getNames()
    {
        return ['apiKey'];
    }
}