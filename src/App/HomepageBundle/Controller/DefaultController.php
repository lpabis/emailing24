<?php

namespace App\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            return new RedirectResponse($this->generateUrl('app_homepage_dashboard_index'));
        }


        return new RedirectResponse($this->generateUrl('app_login'));
    }
}
