<?php

namespace App\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\CampaignBundle\Service\AutoresponderManager;
use App\CampaignBundle\Service\CampaignManager;

class DashboardController extends Controller
{
    /**
     * @Route("/dashboard")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'activeMailingList' => $this->getCampaignManager()->findAllActive(),
            'activeAutoresponderList' => $this->getAutoresponderManager()->findAllActive()
        ];
    }

    /**
     * @return AutoresponderManager
     */
    protected function getAutoresponderManager()
    {
        return $this->get('app_campaign.autoresponder.manager');
    }

    /**
     * @return CampaignManager
     */
    protected function getCampaignManager()
    {
        return $this->get('app_campaign.campaign.manager');
    }
}
