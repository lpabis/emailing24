<?php

namespace App\CampaignBundle\Model;

interface AutoresponderSendingFrequencyInterface
{
    const EVERY_WEEK = 1;
    const EVERY_2_WEEKS = 2;
    const EVERY_3_WEEKS = 3;
    const EVERY_4_WEEKS = 4;
    const EVERY_5_WEEKS = 5;
    const EVERY_6_WEEKS = 6;

    /**
     * @param int $sendingFrequency
     *
     * @return self
     */
    public function setSendingFrequency($sendingFrequency);

    /**
     * @return int
     */
    public function getSendingFrequency();
}
