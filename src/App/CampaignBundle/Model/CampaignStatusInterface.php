<?php

namespace App\CampaignBundle\Model;

interface CampaignStatusInterface
{
    const STATUS_DRAFT = 0;
    const STATUS_NEW = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_PAUSED = 3;
    const STATUS_FINISHED = 4;
    const STATUS_AB_TEST = 5;

    /**
     * @param int $type
     *
     * @return self
     */
    public function setStatus($type);

    /**
     * @return int
     */
    public function getStatus();
}
