<?php

namespace App\CampaignBundle\Model;

interface AutoresponderTypeInterface
{
    const TYPE_CYCLIC = 1;
    const TYPE_SIGNUP = 2;

    /**
     * @param int $type
     *
     * @return self
     */
    public function setType($type);

    /**
     * @return int
     */
    public function getType();
}
