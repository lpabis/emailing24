<?php

namespace App\CampaignBundle\Event;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\EventDispatcher\Event;
use App\CampaignBundle\Entity\Campaign;

class CampaignToMailQueueEvent extends Event
{
    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @var ArrayCollection
     */
    protected $recipients;

    /**
     * @var string
     */
    protected $otherSubject;

    /**
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign, $recipients, $otherSubject = null)
    {
        $this->campaign = $campaign;
        $this->recipients = $recipients;
        $this->otherSubject = $otherSubject;
    }

    /**
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @return ArrayCollection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @return string
     */
    public function getOtherSubject()
    {
        return $this->otherSubject;
    }
}
