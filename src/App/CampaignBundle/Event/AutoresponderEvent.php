<?php

namespace App\CampaignBundle\Event;

use App\CampaignBundle\Entity\Autoresponder;
use Symfony\Component\EventDispatcher\Event;

class AutoresponderEvent extends Event
{
    /**
     * @var Autoresponder
     */
    protected $autoresponder;

    /**
     * @param Autoresponder $autoresponder
     */
    public function __construct(Autoresponder $autoresponder)
    {
        $this->autoresponder = $autoresponder;
    }

    /**
     * @return Autoresponder
     */
    public function getAutoresponder()
    {
        return $this->autoresponder;
    }
}
