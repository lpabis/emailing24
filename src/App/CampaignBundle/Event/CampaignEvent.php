<?php

namespace App\CampaignBundle\Event;

use App\CampaignBundle\Entity\Campaign;
use Symfony\Component\EventDispatcher\Event;

class CampaignEvent extends Event
{
    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}
