<?php

namespace App\CampaignBundle\Service;

use Doctrine\ORM\EntityManager;
use App\CampaignBundle\Entity\CampaignStat;
use App\CampaignBundle\Repository\CampaignStatRepository;

class CampaignStatManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var CampaignStatRepository
     */
    protected $campaignStatRepository;

    /**
     * @param EntityManager          $entityManager
     * @param CampaignStatRepository $campaignStatRepository
     */
    public function __construct(EntityManager $entityManager,
                                CampaignStatRepository $campaignStatRepository)
    {
        $this->entityManager = $entityManager;
        $this->campaignStatRepository = $campaignStatRepository;
    }

    /**
     * @param CampaignStat $campaignStat
     */
    public function saveCampaignStat(CampaignStat $campaignStat)
    {
        $this->entityManager->persist($campaignStat);
        $this->entityManager->flush();
    }
}