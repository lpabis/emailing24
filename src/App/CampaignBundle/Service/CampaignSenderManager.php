<?php

namespace App\CampaignBundle\Service;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManager;
use App\CampaignBundle\Repository\CampaignRepository;
use App\CampaignBundle\Model\CampaignStatusInterface;
use App\CampaignBundle\AppCampaignEvents;
use App\CampaignBundle\Event\CampaignEvent;
use App\CampaignBundle\Entity\Campaign;
use App\RecipientBundle\Entity\Recipient;
use App\RecipientBundle\Entity\RecipientGroup;
use App\CampaignBundle\Event\CampaignToMailQueueEvent;

class CampaignSenderManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var CampaignRepository
     */
    protected $campaignRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EntityManager            $entityManager
     * @param CampaignRepository       $campaignRepository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $entityManager,
                                CampaignRepository $campaignRepository,
                                EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->campaignRepository = $campaignRepository;
        $this->dispatcher = $dispatcher;
    }

    public function startCampaigns()
    {
        $campaigns = $this->campaignRepository->findAllReadyToSend(); //bez autorespondera signup

        foreach($campaigns as $campaign) {

            if(CampaignStatusInterface::STATUS_AB_TEST === $campaign->getStatus()) {

                $this->completeABTestCampaign($campaign);
                continue;
            }

            if(null !== $campaign->getAbTest() && $campaign->getCampaignStat()->getNumberOfCycles() === 0) {

                $this->startABTestCampaign($campaign);
                continue;
            }

            $this->startCampaign($campaign);
        }

        $this->entityManager->flush();
    }

    /**
     * @param RecipientGroup $recipientGroup
     * @param Recipient      $recipient
     */
    public function startAutoresponderSignupCampaigns(RecipientGroup $recipientGroup, Recipient $recipient)
    {
        $campaigns = $this->campaignRepository->findAllReadyToSendForAutoresponderSignup($recipientGroup);

        foreach($campaigns as $campaign) {

            $this->startCampaign($campaign, [$recipient]);
        }
    }

    /**
     * @param Campaign $campaign
     */
    protected function startCampaign(Campaign $campaign, $recipients = null)
    {
        $campaign->setStatus(CampaignStatusInterface::STATUS_IN_PROGRESS);
        $numberOfCycles =  $campaign->getCampaignStat()->getNumberOfCycles() + 1;
        $campaign->getCampaignStat()->setNumberOfCycles($numberOfCycles);

        $event = new CampaignToMailQueueEvent($campaign, null !== $recipients ? $recipients : $campaign->getRecipients());
        $this->dispatcher->dispatch(AppCampaignEvents::CAMPAIGN_CHANGE_STATUS, $event);
    }

    /**
     * @param Campaign $campaign
     */
    protected function startABTestCampaign(Campaign $campaign)
    {
        $campaign->setStatus(CampaignStatusInterface::STATUS_AB_TEST);

        $recipients = $campaign->getRecipients();
        $numberOfAllRecipients = count($recipients);
        $numberOfTestRecipients = round($campaign->getAbTest()->getPercentage()*$numberOfAllRecipients / 100);

        if(($numberOfTestRecipients * 2 ) >= $numberOfAllRecipients) {

            return $this->startCampaign($campaign);
        }

        $event = new CampaignToMailQueueEvent($campaign, $recipients->slice(0, $numberOfTestRecipients), $campaign->getAbTest()->getSubjectA());
        $this->dispatcher->dispatch(AppCampaignEvents::CAMPAIGN_CHANGE_STATUS, $event);

        $event = new CampaignToMailQueueEvent($campaign, $recipients->slice($numberOfTestRecipients, $numberOfTestRecipients), $campaign->getAbTest()->getSubjectB());
        $this->dispatcher->dispatch(AppCampaignEvents::CAMPAIGN_CHANGE_STATUS, $event);
    }

    /**
     * @param Campaign $campaign
     */
    protected function completeABTestCampaign(Campaign $campaign)
    {
        if(time() < ($campaign->getStart()->getTimestamp() + $campaign->getAbTest()->getFinishAfterSeconds())) {

            return;
        }

        $recipients = $campaign->getRecipients();
        $numberOfAllRecipients = count($recipients);
        $numberOfTestRecipients = round($campaign->getAbTest()->getPercentage() * $numberOfAllRecipients / 100);
        $campaign->setSubject($campaign->getAbTest()->getNumberOfOpenedA() < $campaign->getAbTest()->getNumberOfOpenedB() ? $campaign->getAbTest()->getSubjectB() : $campaign->getAbTest()->getSubjectA());

        $this->startCampaign($campaign, $recipients->slice($numberOfTestRecipients * 2));
    }


    public function runCampaign($campaign)
    {
        $campaign->setStatus(CampaignStatusInterface::STATUS_IN_PROGRESS);

        $event = new CampaignEvent($campaign);
        $this->dispatcher->dispatch(AppCampaignEvents::CAMPAIGN_CHANGE_STATUS, $event);

        $this->entityManager->flush();
    }

    public function stopCampaign($campaign)
    {
        $campaign->setStatus(CampaignStatusInterface::STATUS_PAUSED);

        $event = new CampaignEvent($campaign);
        $this->dispatcher->dispatch(AppCampaignEvents::CAMPAIGN_CHANGE_STATUS, $event);

        $this->entityManager->flush();
    }
}