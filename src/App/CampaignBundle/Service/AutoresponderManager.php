<?php

namespace App\CampaignBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\CampaignBundle\Entity\Autoresponder;
use App\CampaignBundle\Repository\AutoresponderRepository;
use App\CampaignBundle\AppCampaignEvents;
use App\CampaignBundle\Event\AutoresponderEvent;

class AutoresponderManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var AutoresponderRepository
     */
    protected $autoresponderRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EntityManager            $entityManager
     * @param AutoresponderRepository  $autoresponderRepository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $entityManager,
                                AutoresponderRepository $autoresponderRepository,
                                EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->autoresponderRepository = $autoresponderRepository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param int $type
     *
     * @return Autoresponder[]|array
     */
    public function findAllByType($type)
    {
        return $this->autoresponderRepository->findBy(['type' => $type]);
    }

    /**
     * @return Autoresponder[]|array
     */
    public function findAllActive()
    {
        return $this->autoresponderRepository->findAllActive();
    }

    /**
     * @param Autoresponder $autoresponder
     */
    public function removeAutoresponder(Autoresponder $autoresponder)
    {
        $this->entityManager->remove($autoresponder);
        $this->entityManager->flush();
    }

    /**
     * @param int $type
     *
     * @return Autoresponder
     */
    public function createAutoresponder($type)
    {
        return new Autoresponder($type);
    }

    /**
     * @param Autoresponder $autoresponder
     *
     */
    public function saveAutoresponder(Autoresponder $autoresponder)
    {
        $autoresponder->setUpdated(new \DateTime());

        $event = new AutoresponderEvent($autoresponder);
        $this->dispatcher->dispatch(AppCampaignEvents::AUTORESPONDER_PRE_UPDATE, $event);

        $this->entityManager->persist($autoresponder);
        $this->entityManager->flush();
    }
}