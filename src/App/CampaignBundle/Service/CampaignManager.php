<?php

namespace App\CampaignBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\CampaignBundle\Entity\Campaign;
use App\CampaignBundle\Repository\CampaignRepository;
use App\CampaignBundle\Event\CampaignEvent;
use App\CampaignBundle\AppCampaignEvents;
use App\CampaignBundle\Model\CampaignStatusInterface;

class CampaignManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var CampaignRepository
     */
    protected $campaignRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EntityManager            $entityManager
     * @param CampaignRepository       $campaignRepository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $entityManager,
                                CampaignRepository $campaignRepository,
                                EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->campaignRepository = $campaignRepository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return Campaign[]|array
     */
    public function findAll()
    {
        return $this->campaignRepository->findBy(['autoresponder' => null]);
    }

    /**
     * @return Campaign[]|array
     */
    public function findAllActive()
    {
        return $this->campaignRepository->findBy(['autoresponder' => null,'status' => CampaignStatusInterface::STATUS_IN_PROGRESS]);
    }

    /**
     * @param Campaign $campaign
     */
    public function removeCampaign(Campaign $campaign)
    {
        $this->entityManager->remove($campaign);
        $this->entityManager->flush();
    }

    /**
     * @return Campaign
     */
    public function createCampaign()
    {
        return new Campaign();
    }

    public function initCampaign(Campaign $campaign)
    {
        $campaign->setUpdated(new \DateTime());
        $this->fillCampaignRecipients($campaign);

        $event = new CampaignEvent($campaign);
        $this->dispatcher->dispatch(AppCampaignEvents::CAMPAIGN_PRE_UPDATE, $event);
    }

    /**
     * @param Campaign $campaign
     *
     */
    public function saveCampaign(Campaign $campaign)
    {
        $this->initCampaign($campaign);

        $this->entityManager->persist($campaign);
        $this->entityManager->flush();
    }

    /**
     * @param Campaign $campaign
     */
    protected function fillCampaignRecipients(Campaign $campaign)
    {
        $campaignRecipients = new ArrayCollection();

        foreach($campaign->getRecipientGroups() as $recipientGroup) {
            foreach($recipientGroup->getRecipients() as $recipient)
            $campaignRecipients->add($recipient);
        }

        $campaign->setRecipients($campaignRecipients);
    }
}