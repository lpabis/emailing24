<?php

namespace App\CampaignBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\RecipientBundle\Service\RecipientGroupManager;
use App\RecipientBundle\Form\DataTransformer\IdsToRecipientGroupsTransformer;

class MailingRecipientsFormType extends AbstractType
{
    /**
     * @var RecipientGroupManager
     */
    protected $recipientGroupManager;

    /**
     * @var IdsToRecipientGroupsTransformer
     */
    protected $idsToRecipientGroupsTransformer;

    /**
     * @param RecipientGroupManager           $recipientGroupManager
     * @param IdsToRecipientGroupsTransformer $idsToRecipientGroupsTransformer
     */
    public function __construct(RecipientGroupManager $recipientGroupManager,
                                IdsToRecipientGroupsTransformer $idsToRecipientGroupsTransformer)
    {
        $this->recipientGroupManager = $recipientGroupManager;
        $this->idsToRecipientGroupsTransformer = $idsToRecipientGroupsTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('recipientGroups', ChoiceType::class, [
            'choices' => $this->prepareChoicesArray(),
            'data_class' => null,
            'required' => true,
            'multiple' => true,
            'expanded' => true,
            'label' => false,
        ]);

        $builder->get('recipientGroups')->addModelTransformer($this->idsToRecipientGroupsTransformer);
    }

    /**
     * @return array
     */
    protected function prepareChoicesArray()
    {
        $choices = [];
        foreach ($this->recipientGroupManager->findAll() as $group) {
            $choices[$group->getName()] = $group->getId();
        }

        return $choices;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'recipientGroups';
    }
}
