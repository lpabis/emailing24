<?php

namespace App\CampaignBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\CampaignBundle\Form\DataTransformer\ArrayToDateTransformer;

class MailingParamsFormType extends AbstractType
{
    /**
     * @var ArrayToDateTransformer
     */
    protected $arrayToDateTransformer;

    /**
     * @param ArrayToDateTransformer $arrayToDateTransformer
     */
    public function __construct(ArrayToDateTransformer $arrayToDateTransformer)
    {
        $this->arrayToDateTransformer = $arrayToDateTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'app_campaign.mailing_params.form.name.label',
                'required' => true
            ])
            ->add('subject', TextType::class,[
                'label' => 'app_campaign.mailing_params.form.subject.label',
                'required' => true
            ])
            ->add('fromName', TextType::class,[
                'label' => 'app_campaign.mailing_params.form.fromName.label',
                'required' => false
            ])
            ->add('fromEmail', EmailType::class,[
                'label' => 'app_campaign.mailing_params.form.fromEmail.label',
                'required' => false
            ])
            ->add('replyEmail', EmailType::class,[
                'label' => 'app_campaign.mailing_params.form.replyEmail.label',
                'required' => false
            ])
            ->add('sendNow', ChoiceType::class, [
                'choices' => [0,1],
                'multiple' => false,
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
            ])
            ->add('start', TextType::class,[
                'required' => false
            ]);

        $builder->get('start')->addModelTransformer($this->arrayToDateTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'params';
    }
}
