<?php

namespace App\CampaignBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;
use App\CampaignBundle\Entity\ABTest;

class MailingABTestFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('isEnabled', ChoiceType::class, [
            'choices' => [
                'no' => 0,
                'yes' => 1,
            ],
            'label' => 'app_campaign.mailing_abTest.form.isABTest.label',
            'data' => 0,
            'expanded' => true,
            'mapped' => false,
        ])->add('subjectA', TextType::class,[
            'label' => 'app_campaign.mailing_abTest.form.subject_a.label',
            'constraints' => new Assert\NotBlank(),
            'required' => false,
        ])->add('subjectB', TextType::class,[
            'label' => 'app_campaign.mailing_abTest.form.subject_b.label',
            'constraints' => new Assert\NotBlank(),
            'required' => false
        ])->add('percentage', NumberType::class,[
            'label' => 'app_campaign.mailing_abTest.form.percentage.label',
            'required' => false,
            'constraints' => new Assert\NotBlank(),
        ])->add('finishAfterSeconds', NumberType::class,[
            'label' => 'app_campaign.mailing_abTest.form.finishAfterSeconds.label',
            'required' => false,
            'constraints' => new Assert\NotBlank(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ABTest::class,
            'validation_groups' => function(FormInterface $form) {
                return $form['isEnabled']->getData() == 1 ? ['Default'] : [];
            },
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'abTest';
    }
}
