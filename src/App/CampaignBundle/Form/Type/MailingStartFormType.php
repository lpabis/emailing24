<?php

namespace App\CampaignBundle\Form\Type;

use App\CampaignBundle\Form\DataTransformer\ArrayToStartTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\DateTime;

class MailingStartFormType extends AbstractType
{
    /**
     * @var ArrayToStartTransformer
     */
    protected $arrayToStartTransformer;

    /**
     * @param ArrayToStartTransformer $arrayToStartTransformer
     */
    public function __construct(ArrayToStartTransformer $arrayToStartTransformer)
    {
        $this->arrayToStartTransformer = $arrayToStartTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('datetime_send', ChoiceType::class, [
                'choices' => [0,1],
                'multiple' => false,
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'mapped' => false,
            ])
            ->add('start', TextType::class,[
                'required' => false
            ]);

        $builder->get('start')->addModelTransformer($this->arrayToStartTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'start';
    }
}
