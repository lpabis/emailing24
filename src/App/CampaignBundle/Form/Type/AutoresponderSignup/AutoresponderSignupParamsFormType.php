<?php

namespace App\CampaignBundle\Form\Type\AutoresponderSignup;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AutoresponderSignupParamsFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('autoresponder', AutoresponderFormType::class, [
                'label'=> false,
            ])
            ->add('subject', TextType::class,[
                'label' => 'app_campaign.autoresponder_params.form.subject.label',
                'required' => true
            ])
            ->add('fromName', TextType::class,[
                'label' => 'app_campaign.autoresponder_params.form.fromName.label',
                'required' => false
            ])
            ->add('fromEmail', EmailType::class,[
                'label' => 'app_campaign.autoresponder_params.form.fromEmail.label',
                'required' => false
            ])
            ->add('replyEmail', EmailType::class,[
                'label' => 'app_campaign.autoresponder_params.form.replyEmail.label',
                'required' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'params';
    }
}
