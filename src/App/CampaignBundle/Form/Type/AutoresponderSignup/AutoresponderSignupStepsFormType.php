<?php

namespace App\CampaignBundle\Form\Type\AutoresponderSignup;

use Symfony\Component\OptionsResolver\OptionsResolver;
use App\CommonBundle\Form\Type\AbstractStepsFormType;
use App\CampaignBundle\Entity\Campaign;

class AutoresponderSignupStepsFormType extends AbstractStepsFormType
{
    /**
     * {@inheritdoc}
     */
    protected function initVarsAttr(array $attr)
    {
        return $attr;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Campaign::class,
            'prefixName' => 'autoresponder_',
        ]);
        $resolver->setRequired(['step']);
        $resolver->setRequired(['routeName']);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'app_campaign_autoresponder_signup_steps';
    }
}
