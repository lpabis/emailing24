<?php

namespace App\CampaignBundle\Form\Type\AutoresponderSignup;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\CampaignBundle\Entity\Autoresponder;
use App\RecipientBundle\Form\DataTransformer\IdsToRecipientGroupsTransformer;
use App\RecipientBundle\Service\RecipientGroupManager;

class AutoresponderFormType extends AbstractType
{
    /**
     * @var RecipientGroupManager
     */
    protected $recipientGroupManager;

    /**
     * @var IdsToRecipientGroupsTransformer
     */
    protected $idsToRecipientGroupsTransformer;

    /**
     * @param RecipientGroupManager           $recipientGroupManager
     * @param IdsToRecipientGroupsTransformer $idsToRecipientGroupsTransformer
     */
    public function __construct(RecipientGroupManager $recipientGroupManager,
                                IdsToRecipientGroupsTransformer $idsToRecipientGroupsTransformer)
    {
        $this->recipientGroupManager = $recipientGroupManager;
        $this->idsToRecipientGroupsTransformer = $idsToRecipientGroupsTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'app_campaign.autoresponder.form.name.label',
                'required' => true
            ])
            ->add('recipientGroups', ChoiceType::class, [
                'choices' => $this->prepareChoicesArray(),
                'data_class' => null,
                'required' => true,
                'multiple' => true,
                'expanded' => true,
                'label' => false,
            ]);

        $builder->get('recipientGroups')->addModelTransformer($this->idsToRecipientGroupsTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Autoresponder::class
        ]);
    }

    /**
     * @return array
     */
    protected function prepareChoicesArray()
    {
        $choices = [];
        foreach ($this->recipientGroupManager->findAll() as $group) {
            $choices[$group->getName()] = $group->getId();
        }

        return $choices;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'autoresponder';
    }
}
