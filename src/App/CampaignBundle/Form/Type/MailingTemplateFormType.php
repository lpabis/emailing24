<?php

namespace App\CampaignBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\TemplateBundle\Service\TemplateManager;
use App\TemplateBundle\Form\DataTransformer\IdToTemplateTransformer;

class MailingTemplateFormType extends AbstractType
{
    /**
     * @var TemplateManager
     */
    protected $templateManager;

    /**
     * @var IdToTemplateTransformer
     */
    protected $idToTemplateTransformer;

    /**
     * @param TemplateManager         $templateManager
     * @param IdToTemplateTransformer $idToTemplateTransformer
     */
    public function __construct(TemplateManager $templateManager,
                                IdToTemplateTransformer $idToTemplateTransformer)
    {
        $this->templateManager = $templateManager;
        $this->idToTemplateTransformer = $idToTemplateTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('template', ChoiceType::class, [
            'choices' => $this->prepareChoicesArray(),
            'data_class' => null,
            'required' => true,
            'multiple' => false,
            'expanded' => true,
            'label' => false,
        ]);

        $builder->get('template')->addModelTransformer($this->idToTemplateTransformer);
    }

    /**
     * @return array
     */
    protected function prepareChoicesArray()
    {
        $choices = [];
        foreach ($this->templateManager->findAll() as $template) {
            $choices[$template->getName()] = $template->getId();
        }

        return $choices;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'template';
    }
}
