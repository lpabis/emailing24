<?php

namespace App\CampaignBundle\Form\Type\AutoresponderCyclic;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\CampaignBundle\Model\AutoresponderSendingFrequencyInterface;
use App\CampaignBundle\Entity\AutoresponderCyclic;

class AutoresponderCyclicFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sendingFrequency', ChoiceType::class, [
            'label' => 'app_campaign.autoresponder_cyclic.form.sendingFrequency.label',
            'choices' => [
                'co 1 tydzień' => AutoresponderSendingFrequencyInterface::EVERY_WEEK,
                'co 2 tygodnie' => AutoresponderSendingFrequencyInterface::EVERY_2_WEEKS,
                'co 3 tygodnie' => AutoresponderSendingFrequencyInterface::EVERY_3_WEEKS,
                'co 4 tygodnie' => AutoresponderSendingFrequencyInterface::EVERY_4_WEEKS,
                'co 5 tygodni' => AutoresponderSendingFrequencyInterface::EVERY_5_WEEKS,
                'co 6 tygodni' => AutoresponderSendingFrequencyInterface::EVERY_6_WEEKS,
            ],
            'multiple' => false,
            'placeholder' => false,
            'expanded' => false,
            'required' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AutoresponderCyclic::class
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'autoresponder_cyclic';
    }
}