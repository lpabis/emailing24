<?php

namespace App\CampaignBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class ArrayToDateTransformer implements DataTransformerInterface
{
    /**
     * @param array $datetime
     *
     * @return \DateTime
     */
    public function reversetransform($datetime)
    {
        if (null === $datetime) {
            return new \DateTime();
        }

        if(null === $datetime['date'] || empty($datetime['date'])) {
            $datetime['date'] = date("d/m/Y");
        }

        if(null === $datetime['time'] || empty($datetime['time'])) {
            $datetime['time'] = date("H:i");
        }

        $datetime = date_format(date_create_from_format('d/m/Y H:i', $datetime['date'] . ' ' . $datetime['time']), 'Y-m-d H:i');

        return new \DateTime($datetime);
    }
    /**
     * @param \DateTime $start
     *
     * @return array
     */
    public function transform($start)
    {
        if (null === $start) {
            return [
                'date' => null,
                'time' => null
            ];
        }

        return [
            'date' => date("d/m/Y", $start->getTimestamp()),
            'time' => date("H:i", $start->getTimestamp()),
        ];
    }
}
