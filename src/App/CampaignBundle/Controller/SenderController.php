<?php

namespace App\CampaignBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\CampaignBundle\Service\CampaignSenderManager;
use App\CampaignBundle\Entity\Campaign;
use App\CampaignBundle\Model\AutoresponderTypeInterface;

class SenderController extends Controller
{
    /**
     * @Route("/sender/start")
     */
    public function startCampaigns()
    {
        $this->getCampaignSenderManager()->startCampaigns();

        die('Completed');
    }

    /**
     * @Route("/run/{campaign}")
     *
     * @param Request  $request
     * @param Campaign $campaign
     *
     * @return RedirectResponse
     */
    public function runAction(Request $request, Campaign $campaign)
    {
        $this->getCampaignSenderManager()->runCampaign($campaign);

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.campaign_sender.message.started.success'));

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/stop/{campaign}")
     *
     * @param Request  $request
     * @param Campaign $campaign
     *
     * @return RedirectResponse
     */
    public function stopAction(Request $request, Campaign $campaign)
    {
        $this->getCampaignSenderManager()->stopCampaign($campaign);

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.campaign_sender.message.stopped.success'));

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @return CampaignSenderManager
     */
    protected function getCampaignSenderManager()
    {
        return $this->get('app_campaign.campaign_sender.manager');
    }
}