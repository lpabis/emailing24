<?php

namespace App\CampaignBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\CampaignBundle\Service\CampaignManager;
use App\CampaignBundle\Form\Type\MailingFormType;
use App\CampaignBundle\Entity\Campaign;
use App\MailQueueBundle\Service\MailQueueManager;

class MailingController extends Controller
{
    /**
     * @Route("/mailing/list")
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        return [
            'list' => $this->getCampaignManager()->findAll(),
        ];
    }

    /**
     * @Route("/mailing/create/{step}", requirements={"step" = "\d+"}, defaults={"step" = 1})
     * @Method({"GET"})
     * @Template("AppCampaignBundle:Mailing:createForm.html.twig")
     *
     * @param Request $request
     * @param int     $step
     *
     * @return array
     */
    public function createFormAction(Request $request, $step)
    {
        $form = $this->get('form.factory')->createNamed('', MailingFormType::class, null, [
            'step' => $step,
            'routeName' => 'app_campaign_mailing_createform',
        ]);

        $form->setData($request->getSession()->get($form->getName()));

        return $this->getResponseData($form->createView(), false);
    }

    /**
     * @Route("/mailing/create/{step}", requirements={"step" = "\d+"}, defaults={"step" = 1})
     * @Method({"POST"})
     * @Template("AppCampaignBundle:Mailing:createForm.html.twig")
     *
     * @param Request $request
     * @param int     $step
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request, $step)
    {
        $form = $this->get('form.factory')->createNamed('', MailingFormType::class, $this->getCampaignManager()->createCampaign(), [
            'step' => $step,
            'routeName' => 'app_campaign_mailing_createform',
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($form->has('test') && $form->get('test')->get('send')->isClicked()) {

                $mailQueue = $this->getMailQueueManager()->createMailQueue($form['test']->getData()['email'], $form->getData());
                $this->getMailQueueManager()->saveMailQueue($mailQueue);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.message.sent.success'));

                return new RedirectResponse($this->generateUrl('app_campaign_mailing_createform', [
                    'step' => $step,
                ]));
            }

            if ($form->get('save')->isClicked()) {

                $this->getCampaignManager()->saveCampaign($form->getData());

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.message.created.success'));

                return new RedirectResponse($this->generateUrl('app_campaign_mailing_list'));
            }

            $request->getSession()->set($form->getName(), $form->getData());
            ++$step;

            return new RedirectResponse($this->generateUrl('app_campaign_mailing_createform', [
                'step' => $step,
            ]));
        }

        if ($form->has('test') && $form->get('test')->get('send')->isClicked()) {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.mailing.message.sent.error'));
        }else {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.mailing.message.created.error'));
        }

        return $this->getResponseData($form->createView(), false);
    }

    /**
     * @Route("/mailing/edit/{campaign}")
     * @Method({"GET"})
     * @Template("AppCampaignBundle:Mailing:editForm.html.twig")
     *
     * @param Campaign $campaign
     *
     * @return array
     */
    public function editFormAction(Campaign $campaign)
    {
        $form = $this->get('form.factory')->createNamed('', MailingFormType::class, $campaign, [
            'step' => null,
            'routeName' => null,
        ]);

        return $this->getResponseData($form->createView(), true);
    }

    /**
     * @Route("/mailing/edit/{campaign}")
     * @Method({"POST"})
     * @Template("AppCampaignBundle:Mailing:editForm.html.twig")
     *
     * @param Request  $request
     * @param Campaign $campaign
     *
     * @return array|RedirectResponse
     */
    public function updateAction(Request $request, Campaign $campaign)
    {
        $form = $this->get('form.factory')->createNamed('', MailingFormType::class, $campaign, [
            'step' => null,
            'routeName' => null,
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($form->get('test')->get('send')->isClicked()) {

                $mailQueue = $this->getMailQueueManager()->createMailQueue($form['test']->getData()['email'], $form->getData());
                $this->getMailQueueManager()->saveMailQueue($mailQueue);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.test.message.sent.success'));

                return $this->getResponseData($form->createView(), true);
            }

            $this->getCampaignManager()->saveCampaign($form->getData());

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_campaign_mailing_list'));
        }

        if ($form->has('test') && $form->get('test')->get('send')->isClicked()) {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.mailing.message.sent.error'));
        }else {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.mailing.message.updated.error'));
        }

        return $this->getResponseData($form->createView(), true);
    }

    /**
     * @Route("/mailing/{campaign}")
     * @Template()
     *
     * @param Campaign
     *
     * @return array
     */
    public function getAction(Campaign $campaign)
    {
        return [
            'campaign' => $campaign,
        ];
    }

    /**
     * @Route("/mailing/delete/{campaign}")
     *
     * @param Campaign $campaign
     *
     * @return RedirectResponse
     */
    public function deleteAction(Campaign $campaign)
    {
        $this->getCampaignManager()->removeCampaign($campaign);

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.message.deleted.success'));

        return new RedirectResponse($this->generateUrl('app_campaign_mailing_list'));
    }

    /**
     * @param FormView $formView
     * @param bool     $isCreated
     *
     * @return array
     */
    protected function getResponseData(FormView $formView, $isCreated)
    {
        return [
            'html' => $this->renderView('AppCampaignBundle:Mailing:' . ($isCreated ? 'edit': 'create') . 'FormContent.html.twig', [
                'form' => $formView,
                'step' => $formView->vars['step'],
                'allForm' => $this->get('form.factory')->createNamed('', MailingFormType::class, null, [
                    'step' => null,
                    'routeName' => null,
                ])
            ]),
            'step' => $formView->vars['step']
        ];
    }

    /**
     * @return CampaignManager
     */
    protected function getCampaignManager()
    {
        return $this->get('app_campaign.campaign.manager');
    }

    /**
     * @return MailQueueManager
     */
    protected function getMailQueueManager()
    {
        return $this->get('app_mail_queue.mail_queue.manager');
    }
}