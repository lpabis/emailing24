<?php

namespace App\CampaignBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\CampaignBundle\Model\AutoresponderTypeInterface;
use App\CampaignBundle\Form\Type\AutoresponderCyclic\AutoresponderCyclicStepsFormType;
use App\CampaignBundle\Service\AutoresponderManager;
use App\CampaignBundle\Service\CampaignManager;
use App\CampaignBundle\Entity\Autoresponder;
use App\MailQueueBundle\Service\MailQueueManager;

class AutoresponderCyclicController extends Controller
{
    /**
     * @Route("/autoresponder/cyclic/list")
     * @Template("AppCampaignBundle:Autoresponder:Cyclic/list.html.twig")
     */
    public function listAction()
    {
        return [
            'list' => $this->getAutoresponderManager()->findAllByType(AutoresponderTypeInterface::TYPE_CYCLIC)
        ];
    }

    /**
     * @Route("/autoresponder/cyclic/create/{step}", requirements={"step" = "\d+"}, defaults={"step" = 1})
     * @Method({"GET"})
     * @Template("AppCampaignBundle:Autoresponder:Cyclic/createForm.html.twig")
     *
     * @param Request $request
     * @param int     $step
     *
     * @return array
     */
    public function createFormAction(Request $request, $step)
    {
        $form = $this->get('form.factory')->createNamed('', AutoresponderCyclicStepsFormType::class, null, [
            'step' => $step,
            'routeName' => 'app_campaign_autorespondercyclic_createform',
        ]);

        $form->setData($request->getSession()->get($form->getName()));

        return $this->getResponseData($form->createView(), false);
    }

    /**
     * @Route("/autoresponder/cyclic/create/{step}", requirements={"step" = "\d+"}, defaults={"step" = 1})
     * @Method({"POST"})
     * @Template("AppCampaignBundle:Autoresponder:Cyclic/createForm.html.twig")
     *
     * @param Request $request
     * @param int     $step
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request, $step)
    {
        $autoresponder = $this->getAutoresponderManager()->createAutoresponder(AutoresponderTypeInterface::TYPE_CYCLIC);
        $campaign = $this->getCampaignManager()->createCampaign();
        $campaign->setAutoresponder($autoresponder);

        $form = $this->get('form.factory')->createNamed('', AutoresponderCyclicStepsFormType::class, $campaign, [
            'step' => $step,
            'routeName' => 'app_campaign_autorespondercyclic_createform',
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($form->has('test') && $form->get('test')->get('send')->isClicked()) {

                $mailQueue = $this->getMailQueueManager()->createMailQueue($form['test']->getData()['email'], $form->getData());
                $this->getMailQueueManager()->saveMailQueue($mailQueue);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.message.sent.success'));

                return new RedirectResponse($this->generateUrl('app_campaign_autorespondercyclic_createform', [
                    'step' => $step,
                ]));
            }

            if ($form->get('save')->isClicked()) {

                $campaign = $form->getData();
                $autoresponder = $campaign->getAutoresponder();
                $campaign->setName($autoresponder->getName());
                $autoresponder->addCampaign($campaign);

                $this->getAutoresponderManager()->saveAutoresponder($autoresponder);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.autoresponder.message.created.success'));

                return new RedirectResponse($this->generateUrl('app_campaign_autorespondercyclic_list'));
            }

            $request->getSession()->set($form->getName(), $form->getData());
            ++$step;

            return new RedirectResponse($this->generateUrl('app_campaign_autorespondercyclic_createform', [
                'step' => $step,
            ]));
        }

        if ($form->has('test') && $form->get('test')->get('send')->isClicked()) {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.mailing.message.sent.error'));
        }else {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.autoresponder.message.created.error'));
        }

        return $this->getResponseData($form->createView(), false);
    }

    /**
     * @Route("/autoresponder/cyclic/edit/{autoresponder}")
     * @Method({"GET"})
     * @Template("AppCampaignBundle:Autoresponder:Cyclic/editForm.html.twig")
     *
     * @param Autoresponder $autoresponder
     *
     * @return array
     */
    public function editFormAction(Autoresponder $autoresponder)
    {
        $form = $this->get('form.factory')->createNamed('', AutoresponderCyclicStepsFormType::class, $autoresponder->getLastCampaign(), [
            'step' => null,
            'routeName' => null,
        ]);

        return $this->getResponseData($form->createView(), true);
    }

    /**
     * @Route("/autoresponder/cyclic/edit/{autoresponder}")
     * @Method({"POST"})
     * @Template("AppCampaignBundle:Autoresponder:Cyclic/editForm.html.twig")
     *
     * @param Request       $request
     * @param Autoresponder $autoresponder
     *
     * @return array|RedirectResponse
     */
    public function updateAction(Request $request, Autoresponder $autoresponder)
    {
        $form = $this->get('form.factory')->createNamed('', AutoresponderCyclicStepsFormType::class, $autoresponder->getLastCampaign(), [
            'step' => null,
            'routeName' => null,
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($form->get('test')->get('send')->isClicked()) {

                $mailQueue = $this->getMailQueueManager()->createMailQueue($form['test']->getData()['email'], $form->getData());
                $this->getMailQueueManager()->saveMailQueue($mailQueue);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.mailing.message.sent.success'));

                return $this->getResponseData($form->createView(), true);
            }

            $campaign = $form->getData();
            $autoresponder = $campaign->getAutoresponder();
            $campaign->setName($autoresponder->getName());

            $this->getAutoresponderManager()->saveAutoresponder($autoresponder);

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.autoresponder.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_campaign_autorespondercyclic_list'));
        }

        if ($form->has('test') && $form->get('test')->get('send')->isClicked()) {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.mailing.message.sent.error'));
        }else {

            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_campaign.autoresponder.message.updated.error'));
        }

        return $this->getResponseData($form->createView(), true);
    }

    /**
     * @Route("/autoresponder/cyclic/{autoresponder}")
     * @Template("AppCampaignBundle:Autoresponder:Cyclic/get.html.twig")
     *
     * @param Autoresponder
     *
     * @return array
     */
    public function getAction(Autoresponder $autoresponder)
    {
        return [
            'autoresponder' => $autoresponder,
        ];
    }

    /**
     * @Route("/autoresponder/cyclic/delete/{autoresponder}")
     *
     * @param Autoresponder $autoresponder
     *
     * @return RedirectResponse
     */
    public function deleteAction(Autoresponder $autoresponder)
    {
        $this->getAutoresponderManager()->removeAutoresponder($autoresponder);

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_campaign.autoresponder.message.deleted.success'));

        return new RedirectResponse($this->generateUrl('app_campaign_autorespondercyclic_list'));
    }

    /**
     * @param FormView $formView
     * @param bool     $isCreated
     *
     * @return array
     */
    protected function getResponseData(FormView $formView, $isCreated)
    {
        return [
            'html' => $this->renderView('AppCampaignBundle:Autoresponder:Cyclic/' . ($isCreated ? 'edit': 'create') . 'FormContent.html.twig', [
                'form' => $formView,
                'step' => $formView->vars['step'],
                'allForm' => $this->get('form.factory')->createNamed('', AutoresponderCyclicStepsFormType::class, null, [
                    'step' => 100,
                    'routeName' => null,
                ])
            ]),
            'step' => $formView->vars['step']
        ];
    }

    /**
     * @return AutoresponderManager
     */
    protected function getAutoresponderManager()
    {
        return $this->get('app_campaign.autoresponder.manager');
    }

    /**
     * @return CampaignManager
     */
    protected function getCampaignManager()
    {
        return $this->get('app_campaign.campaign.manager');
    }

    /**
     * @return MailQueueManager
     */
    protected function getMailQueueManager()
    {
        return $this->get('app_mail_queue.mail_queue.manager');
    }
}