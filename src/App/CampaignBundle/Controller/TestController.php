<?php

namespace App\CampaignBundle\Controller;

use App\CampaignBundle\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TestController extends Controller
{
    /**
     * @Route("/{campaign}/test/send")
     *
     * @param Request  $request
     * @param Campaign $campaign
     */
    public function sendAction(Request $request, Campaign $campaign)
    {

    }
}