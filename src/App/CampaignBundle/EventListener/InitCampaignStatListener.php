<?php
namespace App\CampaignBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\CampaignBundle\AppCampaignEvents;
use App\CampaignBundle\Event\CampaignEvent;
use App\CampaignBundle\Entity\CampaignStat;

class InitCampaignStatListener implements EventSubscriberInterface
{
    /**
     * @param CampaignEvent $event
     */
    public function onPreUpdateCampaign(CampaignEvent $event)
    {
        $campaign = $event->getCampaign();

        if($campaign->getId()) {
            return;
        }

        $campaignStat = new CampaignStat();
        $campaignStat->setNumberOfAll(count($campaign->getRecipients()));
        $campaign->setCampaignStat($campaignStat);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppCampaignEvents::CAMPAIGN_PRE_UPDATE => ['onPreUpdateCampaign']
        ];
    }
}
