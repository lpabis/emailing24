<?php
namespace App\CampaignBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\MailQueueBundle\AppMailQueueEvents;
use App\MailQueueBundle\Event\MailQueueEvent;
use App\CampaignBundle\Model\CampaignStatusInterface;
use App\CampaignBundle\Model\AutoresponderTypeInterface;

class UpdateCampaignStatusListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param MailQueueEvent $event
     */
    public function onAfterSendMail(MailQueueEvent $event)
    {
        $mailQueue = $event->getMailQueue();

        if(null === $mailQueue->getCampaign()) {

            return;
        }

        $campaign = $mailQueue->getCampaign();

        if($campaign->isAutoresponder() && AutoresponderTypeInterface::TYPE_SIGNUP === $campaign->getAutoresponder()->getType()) {

            return;
        }

        if($campaign->isCompleted()) {

            $campaign->setStatus(CampaignStatusInterface::STATUS_FINISHED);
            $this->entityManager->flush($campaign);
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppMailQueueEvents::MAILQUEUE_AFTER_SEND => ['onAfterSendMail', 10]
        ];
    }
}
