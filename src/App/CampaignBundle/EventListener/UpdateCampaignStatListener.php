<?php
namespace App\CampaignBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\MailQueueBundle\AppMailQueueEvents;
use App\MailQueueBundle\Event\MailQueueEvent;
use App\CampaignBundle\Entity\Campaign;
use App\CampaignBundle\Model\CampaignStatusInterface;

class UpdateCampaignStatListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param MailQueueEvent $event
     */
    public function onAfterSendMail(MailQueueEvent $event)
    {
        $mailQueue = $event->getMailQueue();

        if(null === $mailQueue->getCampaign()) {
            return;
        }

        $campaign = $mailQueue->getCampaign();

        $numberOfSent = $campaign->getCampaignStat()->getNumberOfSent() + 1;
        $campaign->getCampaignStat()->setNumberOfSent($numberOfSent);

        /*if($campaign->getCampaignStat()->getNumberOfSent() == $campaign->getCampaignStat()->getNumberOfAll()) {
            $numberOfCycles =  $campaign->getCampaignStat()->getNumberOfCycles() + 1;
            $campaign->getCampaignStat()->setNumberOfCycles($numberOfCycles);
        }*/

        $this->entityManager->flush();
    }

    /**
     * @param MailQueueEvent $event
     */
    public function onAfterOpenMail(MailQueueEvent $event)
    {
        $mailQueue = $event->getMailQueue();

        if(null === $mailQueue->getCampaign()) {
            return;
        }

        $campaign = $mailQueue->getCampaign();

        $this->calculateForCampaignStat($campaign);
        $this->calculateForCampaignABTest($campaign, $mailQueue->getSubject());

        $this->entityManager->flush();
    }

    /**
     * @param Campaign $campaign
     */
    protected function calculateForCampaignStat(Campaign $campaign)
    {
        $numberOfOpened = $campaign->getCampaignStat()->getNumberOfOpened() + 1;
        $campaign->getCampaignStat()->setNumberOfOpened($numberOfOpened);
    }


    protected function calculateForCampaignABTest(Campaign $campaign, $subject)
    {
        if(CampaignStatusInterface::STATUS_AB_TEST !== $campaign->getStatus()) {

            return;
        }

        if($campaign->getAbTest()->getSubjectA() === $subject) {

            $numberOfOpened = $campaign->getAbTest()->getNumberOfOpenedA() + 1;
            $campaign->getAbTest()->setNumberOfOpenedA($numberOfOpened);

            return;
        }

        if($campaign->getAbTest()->getSubjectB() === $subject) {

            $numberOfOpened = $campaign->getAbTest()->getNumberOfOpenedB() + 1;
            $campaign->getAbTest()->setNumberOfOpenedB($numberOfOpened);

            return;
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppMailQueueEvents::MAILQUEUE_AFTER_SEND => ['onAfterSendMail', 20],
            AppMailQueueEvents::MAILQUEUE_AFTER_OPEN => ['onAfterOpenMail', 20],
        ];
    }
}
