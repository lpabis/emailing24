<?php
namespace App\CampaignBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\MailQueueBundle\AppMailQueueEvents;
use App\MailQueueBundle\Event\MailQueueEvent;
use App\CampaignBundle\Model\CampaignStatusInterface;
use App\CampaignBundle\Model\AutoresponderTypeInterface;

class InitCycleAutoresponderListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param MailQueueEvent $event
     */
    public function onAfterSendMail(MailQueueEvent $event)
    {
        $mailQueue = $event->getMailQueue();

        if(null === $mailQueue->getCampaign()) {
            return;
        }

        $campaign = $mailQueue->getCampaign();

        if(!$campaign->isAutoresponder()) {
            return;
        }

        if(AutoresponderTypeInterface::TYPE_CYCLIC != $campaign->getAutoresponder()->getType()) {
            return;
        }

        if(!$campaign->isCompleted()) {
            return;
        }

        $afterDays = 7 * $campaign->getAutoresponder()->getAutoresponderCyclic()->getSendingFrequency();
        $newStart = new \DateTime(date("Y-m-d H:i:s",time()+60*60*24*$afterDays));

        if($newStart > $campaign->getEnd()) {
            return;
        }

        $campaign->setStatus(CampaignStatusInterface::STATUS_NEW);
        $campaign->setStart($newStart);

        $this->entityManager->flush($campaign);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppMailQueueEvents::MAILQUEUE_AFTER_SEND => ['onAfterSendMail', 9]
        ];
    }
}
