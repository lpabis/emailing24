<?php
namespace App\CampaignBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\RecipientBundle\Event\RecipientEvent;
use App\RecipientBundle\AppRecipientEvents;
use App\CampaignBundle\Service\CampaignSenderManager;

class InitSignupAutoresponderListener implements EventSubscriberInterface
{
    /**
     * @var CampaignSenderManager
     */
    protected $campaignSenderManger;

    /**
     * @param CampaignSenderManager $campaignSenderManger
     */
    public function __construct(CampaignSenderManager $campaignSenderManger)
    {
        $this->campaignSenderManger = $campaignSenderManger;
    }

    /**
     * @param RecipientEvent $event
     */
    public function onRecipientPostCreate(RecipientEvent $event)
    {
        $recipient = $event->getRecipient();
        $recipientGroup = $recipient->getRecipientGroup();

        $this->campaignSenderManger->startAutoresponderSignupCampaigns($recipientGroup, $recipient);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppRecipientEvents::RECIPIENT_POST_CREATE => ['onRecipientPostCreate']
        ];
    }
}
