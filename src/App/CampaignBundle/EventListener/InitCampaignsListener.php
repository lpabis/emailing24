<?php
namespace App\CampaignBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\CampaignBundle\AppCampaignEvents;
use App\CampaignBundle\Service\CampaignManager;
use App\CampaignBundle\Event\AutoresponderEvent;

class InitCampaignsListener implements EventSubscriberInterface
{
    /**
     * @var CampaignManager
     */
    protected $campaignManager;

    /**
     * @param CampaignManager $campaignManager
     */
    public function __construct(CampaignManager $campaignManager)
    {
        $this->campaignManager = $campaignManager;
    }

    /**
     * @param AutoresponderEvent $event
     */
    public function onPreUpdateAutoresponder(AutoresponderEvent $event)
    {
        $autoresponder = $event->getAutoresponder();
        foreach($autoresponder->getCampaigns() as $campaign){
            $this->campaignManager->initCampaign($campaign);
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppCampaignEvents::AUTORESPONDER_PRE_UPDATE => ['onPreUpdateAutoresponder']
        ];
    }
}
