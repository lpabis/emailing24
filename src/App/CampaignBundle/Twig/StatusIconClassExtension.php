<?php

namespace App\CampaignBundle\Twig;

use App\CampaignBundle\Model\CampaignStatusInterface;


class StatusIconClassExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('statusIconClass', array($this, 'getStatusIconClass')),
        );
    }

    /**
     * @param int $status
     *
     * @return string
     */
    public function getStatusIconClass($status)
    {
        switch($status) {
            case CampaignStatusInterface::STATUS_NEW:
                return "icon-blue-color";
            case CampaignStatusInterface::STATUS_IN_PROGRESS:
                return "icon-orange-color";
            case CampaignStatusInterface::STATUS_PAUSED:
                return "icon-red-color";
            case CampaignStatusInterface::STATUS_FINISHED:
                return "icon-green-color";
        }

        return $status;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_campaign_status_icon_class_extension';
    }
}
