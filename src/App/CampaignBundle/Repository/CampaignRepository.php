<?php

namespace App\CampaignBundle\Repository;

use Doctrine\ORM\EntityRepository;
use App\CampaignBundle\Entity\Campaign;
use App\CampaignBundle\Model\CampaignStatusInterface;
use App\CampaignBundle\Model\AutoresponderTypeInterface;
use App\RecipientBundle\Entity\RecipientGroup;

class CampaignRepository extends EntityRepository
{
    /**
     * @return array|Campaign[]
     */
    public function findAllReadyToSend()
    {
        $now = new \DateTime(date('Y-m-d H:i:s'));

        return $this->createQueryBuilder('c')
            ->leftJoin('c.autoresponder', 'a')
            ->where('c.status IN (:status)')->setParameter(':status', [CampaignStatusInterface::STATUS_NEW, CampaignStatusInterface::STATUS_AB_TEST])
            ->andWhere('c.autoresponder IS NULL OR a.type NOT IN (:autoresponerType)')->setParameter(':autoresponerType', [AutoresponderTypeInterface::TYPE_SIGNUP])
            ->andWhere('c.start <= :currdate')
            ->andWhere('c.end IS NULL OR c.end >= :currdate')->setParameter(':currdate', $now)
            ->orderBy('c.created', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllReadyToSendForAutoresponderSignup(RecipientGroup $recipientGroup)
    {
        $now = new \DateTime(date('Y-m-d H:i:s'));

        return $this->createQueryBuilder('c')
            ->join('c.autoresponder', 'a')
            ->join('a.recipientGroups', 'rg')
            ->where('a.type IN (:autoresponerType)')->setParameter(':autoresponerType', AutoresponderTypeInterface::TYPE_SIGNUP)
            ->andWhere('c.status IN (:status)')->setParameter(':status', [CampaignStatusInterface::STATUS_IN_PROGRESS])
            ->andWhere('rg = :recipientGroup')->setParameter(':recipientGroup', $recipientGroup)
            ->andWhere('c.start <= :currdate')
            ->andWhere('c.end IS NULL OR c.end >= :currdate')->setParameter(':currdate', $now)
            ->orderBy('c.created', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
