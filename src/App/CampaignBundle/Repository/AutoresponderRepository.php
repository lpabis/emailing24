<?php

namespace App\CampaignBundle\Repository;

use Doctrine\ORM\EntityRepository;
use App\CampaignBundle\Model\CampaignStatusInterface;

class AutoresponderRepository extends EntityRepository
{
    public function findAllActive()
    {
        return $this->createQueryBuilder('a')
            ->join('a.campaigns', 'c')
            ->where('c.status IN (:status)')->setParameter(':status', [CampaignStatusInterface::STATUS_IN_PROGRESS])
            ->orderBy('c.created', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
