<?php

namespace App\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\RecipientBundle\Entity\RecipientGroup;
use App\CampaignBundle\Model\CampaignStatusInterface;
use App\CampaignBundle\Model\AutoresponderTypeInterface;

/**
 * @ORM\Table(name="autoresponder")
 * @ORM\Entity(repositoryClass="App\CampaignBundle\Repository\AutoresponderRepository")
 */
class Autoresponder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", options={"unsigned":true})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var Campaign[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="autoresponder", cascade={"persist","remove"})
     * @ORM\OrderBy({"autoresponderOrder" = "ASC"})
     */
    private $campaigns;

    /**
     * @var RecipientGroup[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\RecipientBundle\Entity\RecipientGroup", cascade={"persist"})
     * @ORM\JoinTable(name="autoresponder_recipient_group",
     *   joinColumns={
     *     @ORM\JoinColumn(name="autoresponder_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="recipient_group_id", referencedColumnName="id")
     *   }
     * )
     */
    private $recipientGroups;

    /**
     * @var AutoresponderCyclic
     *
     * @ORM\OneToOne(targetEntity="AutoresponderCyclic", mappedBy="autoresponder", cascade={"persist","remove"})
     */
    private $autoresponderCyclic;

    /**
     * @param int $type
     */
    public function __construct($type)
    {
        $this->campaigns = new ArrayCollection();
        $this->recipientGroups = new ArrayCollection();
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        switch($this->type) {
            case AutoresponderTypeInterface::TYPE_CYCLIC:
                return 'cyclic';

            case AutoresponderTypeInterface::TYPE_SIGNUP:
                return 'signup';
        }

        return;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        $lastCampaign = $this->getLastCampaign();

        if(null === $lastCampaign) {
            return CampaignStatusInterface::STATUS_NEW;
        }

        return $lastCampaign->getStatus();
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        $lastCampaign = $this->getLastCampaign();

        if(null === $lastCampaign) {
            return 'Wysyłka oczekująca';
        }

        return $lastCampaign->getStatusName();
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->campaigns[0]->getCreated();
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Campaign[]|ArrayCollection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @param Campaign[]|ArrayCollection $campaigns
     */
    public function setCampaigns($campaigns)
    {
        $this->campaigns = $campaigns;
    }

    /**
     * @param Campaign $campaign
     */
    public function addCampaign(Campaign $campaign)
    {
        $campaign->setAutoresponder($this);
        $this->campaigns->add($campaign);
    }

    /**
     * @return Campaign|null
     */
    public function getLastCampaign()
    {
        if(0 === count($this->campaigns)) {
            return;
        }

        return $this->campaigns[0];
    }
    /**
     * @return RecipientGroup[]|ArrayCollection
     */
    public function getRecipientGroups()
    {
        return $this->recipientGroups;
    }

    /**
     * @param RecipientGroup[]|ArrayCollection $recipientGroups
     */
    public function setRecipientGroups($recipientGroups)
    {
        $this->recipientGroups = $recipientGroups;
    }

    /**
     * @return AutoresponderCyclic
     */
    public function getAutoresponderCyclic()
    {
        return $this->autoresponderCyclic;
    }

    /**
     * @param AutoresponderCyclic $autoresponderCyclic
     */
    public function setAutoresponderCyclic($autoresponderCyclic)
    {
        $this->autoresponderCyclic = $autoresponderCyclic;
        $autoresponderCyclic->setAutoresponder($this);
    }
}