<?php

namespace App\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ab_test")
 * @ORM\Entity(repositoryClass="App\CampaignBundle\Repository\ABTestRepository")
 */
class ABTest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject_a", type="string", length=100, nullable=false)
     */
    private $subjectA;

    /**
     * @var string
     *
     * @ORM\Column(name="subject_b", type="string", length=100, nullable=false)
     */
    private $subjectB;

    /**
     * @var int
     *
     * @ORM\Column(name="percentage", type="integer", options={"unsigned":true})
     */
    private $percentage;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_opened_a", type="integer", options={"unsigned":true})
     */
    private $numberOfOpenedA = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_opened_b", type="integer", options={"unsigned":true})
     */
    private $numberOfOpenedB = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="finish_after_seconds", type="integer", options={"unsigned":true})
     */
    private $finishAfterSeconds;

    /**
     * @var Campaign
     *
     * @ORM\OneToOne(targetEntity="Campaign", inversedBy="abTest")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubjectA()
    {
        return $this->subjectA;
    }

    /**
     * @param string $subjectA
     */
    public function setSubjectA($subjectA)
    {
        $this->subjectA = $subjectA;
    }

    /**
     * @return string
     */
    public function getSubjectB()
    {
        return $this->subjectB;
    }

    /**
     * @param string $subjectB
     */
    public function setSubjectB($subjectB)
    {
        $this->subjectB = $subjectB;
    }

    /**
     * @return int
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param int $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return int
     */
    public function getNumberOfOpenedA()
    {
        return $this->numberOfOpenedA;
    }

    /**
     * @param int $numberOfOpenedA
     */
    public function setNumberOfOpenedA($numberOfOpenedA)
    {
        $this->numberOfOpenedA = $numberOfOpenedA;
    }

    /**
     * @return int
     */
    public function getNumberOfOpenedB()
    {
        return $this->numberOfOpenedB;
    }

    /**
     * @param int $numberOfOpenedB
     */
    public function setNumberOfOpenedB($numberOfOpenedB)
    {
        $this->numberOfOpenedB = $numberOfOpenedB;
    }

    /**
     * @return int
     */
    public function getFinishAfterSeconds()
    {
        return $this->finishAfterSeconds;
    }

    /**
     * @param int $finishAfterSeconds
     */
    public function setFinishAfterSeconds($finishAfterSeconds)
    {
        $this->finishAfterSeconds = $finishAfterSeconds;
    }

    /**
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
    }
}