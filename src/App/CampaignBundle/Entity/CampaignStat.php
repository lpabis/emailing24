<?php

namespace App\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignStat.
 *
 * @ORM\Table(name="campaign_stat")
 * @ORM\Entity(repositoryClass="App\CampaignBundle\Repository\CampaignStatRepository")
 */
class CampaignStat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Campaign
     *
     * @ORM\OneToOne(targetEntity="Campaign", inversedBy="campaignStat")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_all", type="integer", length=11, options={"unsigned":true})
     */
    private $numberOfAll = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_sent", type="integer", length=11, options={"unsigned":true})
     */
    private $numberOfSent = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_opened", type="integer", length=11, options={"unsigned":true})
     */
    private $numberOfOpened = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_cycles", type="integer", length=11, options={"unsigned":true})
     */
    private $numberOfCycles = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return int
     */
    public function getNumberOfAll()
    {
        return $this->numberOfAll;
    }

    /**
     * @param int $numberOfAll
     */
    public function setNumberOfAll($numberOfAll)
    {
        $this->numberOfAll = $numberOfAll;
    }

    /**
     * @return int
     */
    public function getNumberOfSent()
    {
        return $this->numberOfSent;
    }

    /**
     * @param int $numberOfSent
     */
    public function setNumberOfSent($numberOfSent)
    {
        $this->numberOfSent = $numberOfSent;
    }

    /**
     * @return int
     */
    public function getNumberOfOpened()
    {
        return $this->numberOfOpened;
    }

    /**
     * @param int $numberOfOpened
     */
    public function setNumberOfOpened($numberOfOpened)
    {
        $this->numberOfOpened = $numberOfOpened;
    }

    /**
     * @return int
     */
    public function getNumberOfCycles()
    {
        return $this->numberOfCycles;
    }

    /**
     * @param int $numberOfCycles
     */
    public function setNumberOfCycles($numberOfCycles)
    {
        $this->numberOfCycles = $numberOfCycles;
    }
}