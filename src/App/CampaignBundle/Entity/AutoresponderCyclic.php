<?php

namespace App\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\CampaignBundle\Model\AutoresponderSendingFrequencyInterface;

/**
 * @ORM\Table(name="autoresponder_cyclic")
 * @ORM\Entity(repositoryClass="App\CampaignBundle\Repository\AutoresponderCyclicRepository")
 */
class AutoresponderCyclic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="sending_frequency", type="integer", options={"unsigned":true})
     */
    private $sendingFrequency;

    /**
     * @var Autoresponder
     *
     * @ORM\OneToOne(targetEntity="Autoresponder", inversedBy="autoresponderCyclic")
     * @ORM\JoinColumn(name="autoresponder_id", referencedColumnName="id")
     */
    private $autoresponder;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSendingFrequency()
    {
        return $this->sendingFrequency;
    }

    /**
     * @param int $sendingFrequency
     */
    public function setSendingFrequency($sendingFrequency)
    {
        $this->sendingFrequency = $sendingFrequency;
    }

    public function getSendingFrequencyName()
    {
        switch($this->sendingFrequency) {
            case AutoresponderSendingFrequencyInterface::EVERY_WEEK:
                return 'Co tydzień';

            case AutoresponderSendingFrequencyInterface::EVERY_2_WEEKS:
                return 'Co 2 tygodnie';

            case AutoresponderSendingFrequencyInterface::EVERY_3_WEEKS:
                return 'Co 3 tygodnie';

            case AutoresponderSendingFrequencyInterface::EVERY_4_WEEKS:
                return 'Co 4 tygodnie';

            case AutoresponderSendingFrequencyInterface::EVERY_5_WEEKS:
                return 'Coo 5 tygodni';

            case AutoresponderSendingFrequencyInterface::EVERY_6_WEEKS:
                return 'Co 6 tygodni';
        }

        return;
    }

    /**
     * @return Autoresponder
     */
    public function getAutoresponder()
    {
        return $this->autoresponder;
    }

    /**
     * @param Autoresponder $autoresponder
     */
    public function setAutoresponder($autoresponder)
    {
        $this->autoresponder = $autoresponder;
    }
}