<?php

namespace App\CampaignBundle\Entity;

use App\CampaignBundle\Model\CampaignStatusInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use App\TemplateBundle\Entity\Template;
use App\RecipientBundle\Entity\Recipient;
use App\RecipientBundle\Entity\RecipientGroup;

/**
 * @ORM\Table(name="campaign")
 * @ORM\Entity(repositoryClass="App\CampaignBundle\Repository\CampaignRepository")
 */
class Campaign
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"unsigned":true})
     */
    private $status = CampaignStatusInterface::STATUS_NEW;

    /**
     * @var Template
     *
     * @ORM\ManyToOne(targetEntity="App\TemplateBundle\Entity\Template")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=150, nullable=false)
     * @Assert\NotBlank
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="from_email", type="string", length=100, nullable=false)
     * @Assert\NotBlank
     */
    private $fromEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=100, nullable=true)
     */
    private $fromName;

    /**
     * @var string
     *
     * @ORM\Column(name="reply_email", type="string", length=100, nullable=true)
     */
    private $replyEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var bool
     */
    private $sendNow;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * @var ABTest
     *
     * @ORM\OneToOne(targetEntity="ABTest", mappedBy="campaign", cascade={"persist","remove"})
     */
    private $abTest;

    /**
     * @var int
     *
     * @ORM\Column(name="autoresponder_order", type="integer", length=4, options={"unsigned":true})
     */
    private $autoresponderOrder = 0;

    /**
     * @var Autoresponder
     *
     * @ORM\ManyToOne(targetEntity="Autoresponder", inversedBy="campaigns")
     * @ORM\JoinColumn(name="autoresponder_id", referencedColumnName="id")
     */
    private $autoresponder;

    /**
     * @var Recipient[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\RecipientBundle\Entity\Recipient", inversedBy="campaigns", cascade={"persist"})
     * @ORM\JoinTable(name="campaign_recipient",
     *   joinColumns={
     *     @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     *   }
     * )
     */
    private $recipients;

    /**
     * @var RecipientGroup[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\RecipientBundle\Entity\RecipientGroup", inversedBy="campaigns", cascade={"persist"})
     * @ORM\JoinTable(name="campaign_recipient_group",
     *   joinColumns={
     *     @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="recipient_group_id", referencedColumnName="id")
     *   }
     * )
     */
    private $recipientGroups;

    /**
     * @var CampaignStat
     *
     * @ORM\OneToOne(targetEntity="CampaignStat", mappedBy="campaign", cascade={"persist","remove"})
     */
    private $campaignStat;

    public function __construct()
    {
        $this->recipients = new ArrayCollection();
        $this->recipientGroups = new ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getStatusName()
    {
        switch($this->status) {
            case CampaignStatusInterface::STATUS_NEW:
                return 'Wysyłka oczekująca';

            case CampaignStatusInterface::STATUS_IN_PROGRESS:
                return 'Wysyłka w toku';

            case CampaignStatusInterface::STATUS_PAUSED:
                return 'Wysyłka zatrzymana';

            case CampaignStatusInterface::STATUS_FINISHED:
                return 'Wysyłka zakończona';

            case CampaignStatusInterface::STATUS_AB_TEST:
                return 'Wysyłka w fazie testów A/B';
        }

        return;
    }

    /**
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param Template $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return int
     */
    public function isAutoresponder()
    {
        return null !== $this->autoresponder;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param string $fromName
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    /**
     * @return string
     */
    public function getReplyEmail()
    {
        return $this->replyEmail;
    }

    /**
     * @param string $replyEmail
     */
    public function setReplyEmail($replyEmail)
    {
        $this->replyEmail = $replyEmail;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return boolean
     */
    public function isSendNow()
    {
        return $this->sendNow;
    }

    /**
     * @param boolean $sendNow
     */
    public function setSendNow($sendNow)
    {
        $this->sendNow = $sendNow;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return ABTest
     */
    public function getAbTest()
    {
        return $this->abTest;
    }

    /**
     * @param ABTest $abTest
     */
    public function setAbTest($abTest)
    {
        if(null === $abTest->getPercentage()) {
            return;
        }

        $this->abTest = $abTest;
        $abTest->setCampaign($this);
    }

    /**
     * @return int
     */
    public function getAutoresponderOrder()
    {
        return $this->autoresponderOrder;
    }

    /**
     * @param int $autoresponderOrder
     */
    public function setAutoresponderOrder($autoresponderOrder)
    {
        $this->autoresponderOrder = $autoresponderOrder;
    }

    /**
     * @return Autoresponder
     */
    public function getAutoresponder()
    {
        return $this->autoresponder;
    }

    /**
     * @param Autoresponder $autoresponder
     */
    public function setAutoresponder($autoresponder)
    {
        $this->autoresponder = $autoresponder;
    }

    /**
     * @return Recipient[]|ArrayCollection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param Recipient[]|ArrayCollection $recipients
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    }

    /**
     * @return RecipientGroup[]|ArrayCollection
     */
    public function getRecipientGroups()
    {
        return $this->recipientGroups;
    }

    /**
     * @param \App\RecipientBundle\Entity\RecipientGroup[]|ArrayCollection $recipientGroups
     */
    public function setRecipientGroups($recipientGroups)
    {
        $this->recipientGroups = $recipientGroups;
    }

    /**
     * @return CampaignStat
     */
    public function getCampaignStat()
    {
        return $this->campaignStat;
    }

    /**
     * @param CampaignStat $campaignStat
     */
    public function setCampaignStat($campaignStat)
    {
        $this->campaignStat = $campaignStat;
        $campaignStat->setCampaign($this);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return CampaignStatusInterface::STATUS_IN_PROGRESS === $this->status;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return null !== $this->campaignStat && $this->campaignStat->getNumberOfSent() >= $this->campaignStat->getNumberOfAll();
    }
}