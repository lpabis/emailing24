<?php

namespace App\CampaignBundle;

final class AppCampaignEvents
{
    const AUTORESPONDER_PRE_UPDATE = 'app_campaign.autoresponder.pre_update';
    const CAMPAIGN_PRE_UPDATE = 'app_campaign.campaign.pre_update';
    const CAMPAIGN_CHANGE_STATUS = 'app_campaign.campaign.change_status';
}
