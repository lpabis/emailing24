<?php

namespace App\MailQueueBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\CampaignBundle\AppCampaignEvents;
use App\CampaignBundle\Event\CampaignToMailQueueEvent;
use App\MailQueueBundle\Service\MailQueueManager;
use App\CampaignBundle\Model\CampaignStatusInterface;

class CreateMailQueueListener implements EventSubscriberInterface
{
    /**
     * @var MailQueueManager
     */
    protected $mailQueueManager;

    /**
     * @param MailQueueManager $mailQueueManager
     */
    public function __construct(MailQueueManager $mailQueueManager)
    {
        $this->mailQueueManager = $mailQueueManager;
    }

    /**
     * @param CampaignToMailQueueEvent $event
     */
    public function onCampaignChangeStatus(CampaignToMailQueueEvent $event)
    {
        $campaign = $event->getCampaign();
        $recipients = $event->getRecipients();
        $otherSubject = $event->getOtherSubject();

        if(CampaignStatusInterface::STATUS_IN_PROGRESS !== $campaign->getStatus() &&
           CampaignStatusInterface::STATUS_AB_TEST !== $campaign->getStatus()) {
                return;
        }

        foreach($recipients as $recipient) {

            $mailQueue = $this->mailQueueManager->createMailQueue($recipient, $campaign, $otherSubject);
            $this->mailQueueManager->saveMailQueue($mailQueue);
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppCampaignEvents::CAMPAIGN_CHANGE_STATUS => ['onCampaignChangeStatus']
        ];
    }
}
