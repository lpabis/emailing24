<?php

namespace App\MailQueueBundle\Model;

interface MailQueueStatusInterface
{
    const STATUS_NEW = 0;
    const STATUS_PAUSED = 1;
    const STATUS_SENDING = 2;
    const STATUS_FINISHED = 3;

    /**
     * @param int $type
     *
     * @return self
     */
    public function setStatus($type);

    /**
     * @return int
     */
    public function getStatus();
}
