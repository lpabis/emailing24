<?php

namespace App\MailQueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MailReaction.
 *
 * @ORM\Table(name="mail_reaction")
 * @ORM\Entity(repositoryClass="App\MailQueueBundle\Repository\MailReactionRepository")
 */
class MailReaction
{
    /**
     * @var MailQueue
     *
     * @ORM\OneToOne(targetEntity="MailQueue", inversedBy="mailQueue")
     * @ORM\JoinColumn(name="mail_queue_id", referencedColumnName="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mailQueue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="opened", type="datetime", nullable=true)
     */
    private $opened;

    public function __construct(MailQueue $mailQueue)
    {
        $this->mailQueue = $mailQueue;
    }

    /**
     * @return MailQueue
     */
    public function getMailQueue()
    {
        return $this->mailQueue;
    }

    /**
     * @param MailQueue $mailQueue
     */
    public function setMailQueue($mailQueue)
    {
        $this->mailQueue = $mailQueue;
    }

    /**
     * @return \DateTime
     */
    public function getOpened()
    {
        return $this->opened;
    }

    /**
     * @param \DateTime $opened
     */
    public function setOpened($opened)
    {
        $this->opened = $opened;
    }
}