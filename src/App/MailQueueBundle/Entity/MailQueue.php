<?php

namespace App\MailQueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\CampaignBundle\Entity\Campaign;
use App\RecipientBundle\Entity\Recipient;
use App\MailQueueBundle\Model\MailQueueStatusInterface;

/**
 * MailQueue.
 *
 * @ORM\Table(name="mail_queue")
 * @ORM\Entity(repositoryClass="App\MailQueueBundle\Repository\MailQueueRepository")
 */
class MailQueue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=100, nullable=true)
     */
    private $fromName;

    /**
     * @var string
     *
     * @ORM\Column(name="from_email", type="string", length=100, nullable=false)
     */
    private $fromEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="to_name", type="string", length=100, nullable=true)
     */
    private $toName;

    /**
     * @var string
     *
     * @ORM\Column(name="to_email", type="string", length=100, nullable=false)
     */
    private $toEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="reply_email", type="string", length=100, nullable=false)
     */
    private $replyEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=150, nullable=false)
     */
    private $subject;

    /**
     * @var text
     *
     * @ORM\Column(name="html_body", type="text", nullable=false)
     */
    private $htmlBody;

    /**
     * @var text
     *
     * @ORM\Column(name="text_body", type="text", nullable=true)
     */
    private $textBody;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=false)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent", type="datetime", nullable=false)
     */
    private $sent;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"unsigned":true})
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="prioritet", type="integer", options={"unsigned":true})
     */
    private $prioritet;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=20, nullable=false)
     */
    private $hash;

    /**
     * @var Campaign
     *
     * @ORM\ManyToOne(targetEntity="App\CampaignBundle\Entity\Campaign")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;

    /**
     * @var Recipient
     *
     * @ORM\ManyToOne(targetEntity="App\RecipientBundle\Entity\Recipient", inversedBy="mailQueues")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     */
    private $recipient;

    /**
     * @var MailReaction
     *
     * @ORM\OneToOne(targetEntity="MailReaction", mappedBy="mailQueue", cascade={"persist","remove"})
     */
    private $mailReaction;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->status = MailQueueStatusInterface::STATUS_NEW;
        $this->hash = substr(md5(mt_rand()), 0, 20);
        $this->prioritet = 1;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param string $fromName
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    /**
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string
     */
    public function getToName()
    {
        return $this->toName;
    }

    /**
     * @param string $toName
     */
    public function setToName($toName)
    {
        $this->toName = $toName;
    }

    /**
     * @return string
     */
    public function getToEmail()
    {
        return $this->toEmail;
    }

    /**
     * @param string $toEmail
     */
    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;
    }

    /**
     * @return string
     */
    public function getReplyEmail()
    {
        return $this->replyEmail;
    }

    /**
     * @param string $replyEmail
     */
    public function setReplyEmail($replyEmail)
    {
        $this->replyEmail = $replyEmail;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return text
     */
    public function getHtmlBody()
    {
        return $this->htmlBody;
    }

    /**
     * @param text $htmlBody
     */
    public function setHtmlBody($htmlBody)
    {
        $this->htmlBody = $htmlBody;
    }

    /**
     * @return text
     */
    public function getTextBody()
    {
        return $this->textBody;
    }

    /**
     * @param text $textBody
     */
    public function setTextBody($textBody)
    {
        $this->textBody = $textBody;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }


    /**
     * @return \DateTime
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * @param \DateTime $sent
     */
    public function setSent($sent)
    {
        $this->sent = $sent;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getPrioritet()
    {
        return $this->prioritet;
    }

    /**
     * @param int $prioritet
     */
    public function setPrioritet($prioritet)
    {
        $this->prioritet = $prioritet;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param Recipient $recipient
     */
    public function setRecipient(Recipient $recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return MailReaction
     */
    public function getMailReaction()
    {
        return $this->mailReaction;
    }

    /**
     * @param MailReaction $mailReaction
     */
    public function setMailReaction($mailReaction)
    {
        $this->mailReaction = $mailReaction;
        $mailReaction->setMailQueue($this);
    }
}