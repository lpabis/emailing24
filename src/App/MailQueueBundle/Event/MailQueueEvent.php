<?php

namespace App\MailQueueBundle\Event;

use App\MailQueueBundle\Entity\MailQueue;
use Symfony\Component\EventDispatcher\Event;

class MailQueueEvent extends Event
{
    /**
     * @var MailQueue
     */
    protected $mailQueue;

    /**
     * @param MailQueue $mailQueue
     */
    public function __construct(MailQueue $mailQueue)
    {
        $this->mailQueue = $mailQueue;
    }

    /**
     * @return MailQueue
     */
    public function getMailQueue()
    {
        return $this->mailQueue;
    }
}
