<?php

namespace App\MailQueueBundle;

final class AppMailQueueEvents
{
    const MAILQUEUE_AFTER_SEND = 'app_mail_queue.mail_queue.after_send';
    const MAILQUEUE_AFTER_OPEN = 'app_mail_queue.mail_queue.after_open';
}
