<?php

namespace App\MailQueueBundle\Repository;

use Doctrine\ORM\EntityRepository;
use App\MailQueueBundle\Entity\MailQueue;
use App\MailQueueBundle\Model\MailQueueStatusInterface;

class MailQueueRepository extends EntityRepository
{
    /**
     * @return array|MailQueue[]
     */
    public function findAllReadyToSend()
    {
        $now = new \DateTime(date('Y-m-d H:i:s'));

        return $this->createQueryBuilder('mq')
            ->where('mq.status = :status')->setParameter(':status', MailQueueStatusInterface::STATUS_NEW)
            ->andWhere('mq.start <= :currdate')
            ->andWhere('mq.end IS NULL OR mq.end >= :currdate')->setParameter(':currdate', $now)
            ->orderBy('mq.prioritet', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $hash
     *
     * @return MailQueue
     */
    public function findByHash($hash)
    {
        return $this->findOneBy(['hash' => $hash]);
    }
}
