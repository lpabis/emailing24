<?php

namespace App\MailQueueBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\MailQueueBundle\Entity\MailQueue;
use App\SettingBundle\Service\SettingManager;
use App\MailQueueBundle\Model\MailQueueStatusInterface;
use App\MailQueueBundle\AppMailQueueEvents;
use App\MailQueueBundle\Event\MailQueueEvent;

class MailSenderManager
{
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var SettingManager
     */
    protected $settingManager;

    /**
     * @param EventDispatcherInterface $dispatcher
     * @param EntityManager            $entityManager
     * @param SettingManager           $settingManager
     */
    public function __construct(EventDispatcherInterface $dispatcher,
                                EntityManager $entityManager,
                                SettingManager $settingManager)
    {
        $this->dispatcher = $dispatcher;
        $this->entityManager = $entityManager;
        $this->settingManager = $settingManager;
    }

    /**
     * @param MailQueue $mailQueue
     */
    public function send(MailQueue $mailQueue)
    {
        $smtpSettings = $this->settingManager->getSettings('smtp');

        $mailQueue->setStatus(MailQueueStatusInterface::STATUS_SENDING);
        $mailQueue->setSent(new \DateTime());

        $this->entityManager->flush($mailQueue);

        $transport = \Swift_SmtpTransport::newInstance($smtpSettings->getMailerHost(), $smtpSettings->getMailerPort())
            ->setUsername($smtpSettings->getMailerUsername())
            ->setPassword($smtpSettings->getMailerPassword())
            ->setAuthMode("login")
            ->setEncryption("ssl");

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance()
            ->setSubject($mailQueue->getSubject())
            ->setFrom($mailQueue->getFromEmail())
            ->setTo($mailQueue->getToEmail())
            ->setBody($mailQueue->getHtmlBody())
            ->setContentType('text/html');

        $mailer->send($message);

        $mailQueue->setStatus(MailQueueStatusInterface::STATUS_FINISHED);

        $this->entityManager->flush();

        $event = new MailQueueEvent($mailQueue);
        $this->dispatcher->dispatch(AppMailQueueEvents::MAILQUEUE_AFTER_SEND, $event);
    }
}