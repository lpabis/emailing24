<?php

namespace App\MailQueueBundle\Service;

use App\MailQueueBundle\Entity\MailQueue;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class MailContentManager
{
    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param MailQueue $mailQueue
     *
     * @return string
     */
    public function createContent(MailQueue $mailQueue)
    {
        $campaign = $mailQueue->getCampaign();
        $recipient = $mailQueue->getRecipient();

        $html = $campaign->getTemplate()->getHtmlVersion();
        $html = $this->replaceTagWithValue($html, '$$email$$', $recipient->getEmail());
        $html = $this->replaceTagsWithAdditionalDataValues($html, $recipient->getAdditionalDataValues());
        $html = $this->appendOpenedCounter($html, $mailQueue->getHash());

        $mailQueue->setHtmlBody($html);

        return $html;
    }

    /**
     * @param string                                    $html
     * @param RecipientAdditionalData[]|ArrayCollection $recipientAdditionalDataValues
     */
    protected function replaceTagsWithAdditionalDataValues($html, $recipientAdditionalDataValues)
    {
        foreach($recipientAdditionalDataValues as $recipientAdditionalDataValue) {
            $html = $this->replaceTagWithValue($html, $recipientAdditionalDataValue->getAdditionalData()->getTag(), $recipientAdditionalDataValue->getValue());
        }

        return $html;
    }

    protected function replaceTagWithValue($html,$tagName, $value)
    {
        return str_replace($tagName, $value, $html);
    }

    protected function appendOpenedCounter($html, $mailHash)
    {
        $url = "http://".$_SERVER['SERVER_NAME'].$this->router->generate('app_mailqueue_reaction_open',['mailQueue' => $mailHash]);

        $html .= "<img src='{$url}' alt='' width='1px' height='1px' style='border:none'/>";

        return $html;
    }
}