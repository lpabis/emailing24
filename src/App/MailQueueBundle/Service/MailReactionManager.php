<?php

namespace App\MailQueueBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\MailQueueBundle\Entity\MailReaction;
use App\MailQueueBundle\Entity\MailQueue;
use App\MailQueueBundle\Event\MailQueueEvent;
use App\MailQueueBundle\AppMailQueueEvents;

class MailReactionManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EntityManager       $entityManager
     */
    public function __construct(EntityManager $entityManager,
                                EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param MailQueue $mailQueue
     *
     * @return MailReaction
     */
    public function createMailReaction(MailQueue $mailQueue)
    {
        return new MailReaction($mailQueue);
    }

    /**
     * @param MailReaction $mailReaction
     */
    public function logOpening(MailReaction $mailReaction)
    {
        $mailReaction->setOpened(new \DateTime());

        $this->saveMailReaction($mailReaction);

        $event = new MailQueueEvent($mailReaction->getMailQueue());
        $this->dispatcher->dispatch(AppMailQueueEvents::MAILQUEUE_AFTER_OPEN, $event);
    }

    /**
     * @param MailReaction $mailReaction
     */
    public function saveMailReaction(MailReaction $mailReaction)
    {
        $this->entityManager->persist($mailReaction);
        $this->entityManager->flush();
    }
}