<?php

namespace App\MailQueueBundle\Service;

use Doctrine\ORM\EntityManager;
use App\MailQueueBundle\Repository\MailQueueRepository;
use App\MailQueueBundle\Entity\MailQueue;
use App\RecipientBundle\Entity\Recipient;
use App\CampaignBundle\Entity\Campaign;

class MailQueueManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var MailQueueRepository
     */
    protected $mailQueueRepository;

    /**
     * @var MailContentManager
     */
    protected $mailContentManager;

    /**
     * @param EntityManager       $entityManager
     * @param MailQueueRepository $mailQueueRepository
     * @param MailContentManager  $mailContentManager
     */
    public function __construct(EntityManager $entityManager,
                                MailQueueRepository $mailQueueRepository,
                                MailContentManager $mailContentManager)
    {
        $this->entityManager = $entityManager;
        $this->mailQueueRepository = $mailQueueRepository;
        $this->mailContentManager = $mailContentManager;
    }

    public function findAllReadyToSend()
    {
        return $this->mailQueueRepository->findAllReadyToSend();
    }

    /**
     * @param Recipient|string   $recipient
     * @param Campaign           $campaign
     * @param string|null        $otherSubject
     * @param bool               $forTests
     *
     * @return MailQueue
     */
    public function createMailQueue($recipient, Campaign $campaign, $otherSubject = null)
    {
        $mailQueue = new MailQueue();

        $mailQueue->setFromName($campaign->getFromName());
        $mailQueue->setFromEmail($campaign->getFromEmail());
        $mailQueue->setReplyEmail($campaign->getReplyEmail());
        $mailQueue->setSubject(null !== $otherSubject ? $otherSubject : $campaign->getSubject());
        $mailQueue->setHtmlBody($campaign->getTemplate()->getHtmlVersion());
        $mailQueue->setStart($campaign->getStart());
        $mailQueue->setEnd($campaign->getEnd());

        /**FOR TESTING**/
        if(is_string($recipient)) {

            $mailQueue->setToEmail($recipient);
            $mailQueue->setStart(new \DateTime());
            $mailQueue->setEnd(null);

            return $mailQueue;
        }

        $mailQueue->setToEmail($recipient->getEmail());
        $mailQueue->setCampaign($campaign);
        $mailQueue->setRecipient($recipient);

        $this->mailContentManager->createContent($mailQueue);

        return $mailQueue;
    }

    /**
     * @param MailQueue $mailQueue
     */
    public function saveMailQueue(MailQueue $mailQueue)
    {
        $this->entityManager->persist($mailQueue);
        $this->entityManager->flush();
    }
}