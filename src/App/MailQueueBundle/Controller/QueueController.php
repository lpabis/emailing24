<?php

namespace App\MailQueueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\MailQueueBundle\Service\MailSenderManager;
use App\MailQueueBundle\Service\MailQueueManager;

class QueueController extends Controller
{
    /**
     * @Route("/send")
     */
    public function sendAction()
    {
        $mails = $this->getMailQueueManager()->findAllReadyToSend();
        foreach($mails as $mail) {
            $this->getMailSenderManager()->send($mail);
        }

        die('Completed');
    }

    /**
     * @return MailQueueManager
     */
    protected function getMailQueueManager()
    {
        return $this->get('app_mail_queue.mail_queue.manager');
    }

    /**
     * @return MailSenderManager
     */
    protected function getMailSenderManager()
    {
        return $this->get('app_mail_queue.mail_sender.manager');
    }
}