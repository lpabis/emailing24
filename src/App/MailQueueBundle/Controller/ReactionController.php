<?php

namespace App\MailQueueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\MailQueueBundle\Entity\MailQueue;
use App\MailQueueBundle\Service\MailReactionManager;

class ReactionController extends Controller
{
    /**
     * @Route("/reaction/open/{mailQueue}")
     *
     * @ParamConverter("mailQueue", class="AppMailQueueBundle:MailQueue", options={"repository_method" = "findByHash"})
     *
     * @param MailQueue
     */
    public function openAction(MailQueue $mailQueue)
    {
        if(null === $mailQueue->getMailReaction()) {

            $mailReaction = $this->getMailReactionManager()->createMailReaction($mailQueue);
            $this->getMailReactionManager()->logOpening($mailReaction);
        }

        die("Completed");
    }

    /**
     * @return MailReactionManager
     */
    protected function getMailReactionManager()
    {
        return $this->get('app_mail_queue.mail_reaction.manager');
    }
}