<?php

namespace App\TemplateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Template.
 *
 * @ORM\Table(name="template")
 * @ORM\Entity(repositoryClass="App\TemplateBundle\Repository\TemplateRepository")
 */
class Template
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"api"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank
     * @Serializer\Groups({"api"})
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=20, nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $hash;

    /**
     * @var text
     *
     * @ORM\Column(name="html_version", type="text", nullable=true)
     * @Assert\NotBlank
     * @Serializer\Groups({"api"})
     */
    private $htmlVersion;

    /**
     * @var text
     *
     * @ORM\Column(name="text_version", type="text", nullable=true)
     */
    private $textVersion;

    public function __construct()
    {
        $this->updated = new \DateTime();
        $this->hash = substr(md5(mt_rand()), 0, 20);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return text
     */
    public function getHtmlVersion()
    {
        return $this->htmlVersion;
    }

    /**
     * @param text $htmlVersion
     */
    public function setHtmlVersion($htmlVersion)
    {
        $this->htmlVersion = $htmlVersion;
    }

    /**
     * @return text
     */
    public function getTextVersion()
    {
        return $this->textVersion;
    }

    /**
     * @param text $textVersion
     */
    public function setTextVersion($textVersion)
    {
        $this->textVersion = $textVersion;
    }
}