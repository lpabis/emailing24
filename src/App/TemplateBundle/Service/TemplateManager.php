<?php

namespace App\TemplateBundle\Service;

use Doctrine\ORM\EntityManager;
use App\TemplateBundle\Entity\Template;
use App\TemplateBundle\Repository\TemplateRepository;

class TemplateManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TemplateRepository
     */
    protected $templateRepository;

    /**
     * @param EntityManager      $entityManager
     * @param TemplateRepository $templateRepository
     */
    public function __construct(EntityManager $entityManager,
                                TemplateRepository $templateRepository)
    {
        $this->entityManager = $entityManager;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param int $id
     *
     * @return Template
     */
    public function find($id)
    {
        return $this->templateRepository->find($id);
    }

    /**
     * @return Template[]|array
     */
    public function findAll()
    {
        return $this->templateRepository->findAll();
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        $this->entityManager->remove($template);
        $this->entityManager->flush($template);
    }

    /**
     * @param Template $template
     * @param bool     $allReferencedEntities
     */
    public function saveTemplate(Template $template, $allReferencedEntities = false)
    {
        $this->entityManager->persist($template);
        $this->entityManager->flush(!$allReferencedEntities ? $template : null);

    }
}