<?php

namespace App\TemplateBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use App\TemplateBundle\Service\TemplateManager;
use App\TemplateBundle\Entity\Template;

class IdToTemplateTransformer implements DataTransformerInterface
{
    /**
     * @var TemplateManager
     */
    protected $templateManager;

    /**
     * @param TemplateManager $templateManager
     */
    public function __construct(TemplateManager $templateManager)
    {
        $this->templateManager = $templateManager;
    }

    /**
     * @param int $id
     *
     * @return Template
     */
    public function reversetransform($id)
    {
        if (null === $id) {
            return;
        }

        return $this->templateManager->find($id);
    }

    /**
     * @param Template $template
     *
     * @return int
     */
    public function transform($template)
    {
        if (null === $template) {
            return;
        }

        return $template->getId();
    }
}
