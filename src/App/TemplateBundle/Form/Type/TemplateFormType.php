<?php

namespace App\TemplateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use App\TemplateBundle\Entity\Template;

class TemplateFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'app_template.template.form.name.label',
                'required' => true
            ])
            ->add('htmlVersion', TextareaType::class,[
                'label' => 'app_template.template.form.html_version.label',
                'required' => true
            ]);
            /*->add('textVersion', TextareaType::class, [
                'label' => 'app_template.template.form.text_version.label',
                'required' => false,
            ]);*/
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Template::class,
            'csrf_protection' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_template_template';
    }
}
