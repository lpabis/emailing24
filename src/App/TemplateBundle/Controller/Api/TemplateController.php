<?php

namespace App\TemplateBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use App\TemplateBundle\Form\Type\TemplateFormType;
use App\TemplateBundle\Service\TemplateManager;
use App\TemplateBundle\Entity\Template as MailTemplate;

/**
 * @RouteResource("/api/template", pluralize=false)
 */
class TemplateController extends FOSRestController
{
    /**
     * @ApiDoc(
     *   section = "Szablony treści",
     *   output = {
     *     "class" = "App\TemplateBundle\Entity\Template",
     *     "groups" = {"api"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @return View
     */
    public function getListAction()
    {
        return View::create()
            ->setStatusCode(200)
            ->setContext($this->getSerializationContext())
            ->setData([
                'list' => $this->getTemplateManager()->findAll(),
            ]);
    }

     /**
      * @ApiDoc(
      *   section = "Szablony treści",
      *   input = {
      *     "class" = "App\TemplateBundle\Form\Type\TemplateFormType",
      *     "name" = "",
      *   },
      *   output = {
      *     "class" = "App\TemplateBundle\Entity\Template",
      *     "groups" = {"api"},
      *   },
      *   statusCodes = {
      *     201 = "Returned when successful",
      *     400 = "Returned when the form has errors"
      *   }
      * )
      *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed('', TemplateFormType::class, null, [
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        $viewData = $form;

        if ($form->isValid()) {
            $this->getTemplateManager()->saveTemplate($form->getData());
            $viewData = $form->getData();

            return View::create()
                ->setStatusCode(201)
                ->setContext($this->getSerializationContext())
                ->setData($viewData);

        }

        return View::create()
            ->setStatusCode(400)
            ->setContext($this->getSerializationContext())
            ->setData($viewData);
    }

    /**
     * @ApiDoc(
     *   section = "Szablony treści",
     *   input = {
     *     "class" = "App\TemplateBundle\Form\Type\TemplateFormType",
     *     "name" = "",
     *   },
     *   output = {
     *     "class" = "App\TemplateBundle\Entity\Template",
     *     "groups" = {"api"},
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request      $request
     * @param MailTemplate $template
     *
     * @return View
     */
    public function patchAction(Request $request, MailTemplate $template)
    {
        $form = $this->get('form.factory')->createNamed('', TemplateFormType::class, $template, [
            'csrf_protection' => false,
            'method' => 'PATCH',
        ]);

        $form->handleRequest($request);

        $viewData = $form;

        if ($form->isValid()) {
            $this->getTemplateManager()->saveTemplate($template);
            $viewData = $template;

            return View::create()
                ->setStatusCode(200)
                ->setContext($this->getSerializationContext())
                ->setData($viewData);
        }

        return View::create()
            ->setStatusCode(400)
            ->setContext($this->getSerializationContext())
            ->setData($viewData);
    }

    /**
     * @ApiDoc(
     *   section = "Szablony treści",
     *   statusCodes={
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param MailTemplate $template
     *
     * @return View
     */
    public function deleteAction(MailTemplate $template)
    {
        $this->getTemplateManager()->removeTemplate($template);

        return View::create()
            ->setStatusCode(204);
    }

    /**
     * @return TemplateManager
     */
    protected function getTemplateManager()
    {
        return $this->get('app_template.template.manager');
    }

    /**
     * @return Context
     */
    protected function getSerializationContext()
    {
        $context = new Context();
        $context->setSerializeNull(true);
        $context->setGroups(array('api'));

        return $context;
    }
}