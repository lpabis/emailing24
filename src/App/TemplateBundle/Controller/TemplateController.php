<?php

namespace App\TemplateBundle\Controller;

use App\RecipientBundle\Entity\AdditionalData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use App\TemplateBundle\Service\TemplateManager;
use App\TemplateBundle\Form\Type\TemplateFormType;
use App\TemplateBundle\Entity\Template as MailTemplate;
use App\RecipientBundle\Service\AdditionalDataManager;

class TemplateController extends Controller
{
    /**
     * @Route("/list")
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        return [
            'list' => $this->getTemplateManager()->findAll(),
        ];
    }

    /**
     * @Route("/get/{template}")
     * @Template()
     *
     * @param MailTemplate $template
     *
     * @return array
     */
    public function getAction(MailTemplate $template)
    {
        return [
            'template' => $template
        ];
    }

    /**
     * @Route("/preview/{template}")
     * @Template()
     *
     * @param MailTemplate $template
     *
     * @return array
     */
    public function previewAction(MailTemplate $template)
    {
        return [
            'template' => $template
        ];
    }

    /**
     * @Route("/create")
     * @Method({"GET"})
     * @Template("AppTemplateBundle:Template:createForm.html.twig")
     *
     * @return array
     */
    public function getCreateFormAction()
    {
        $form = $this->get('form.factory')->createNamed('', TemplateFormType::class);

        return [
            'form' => $form,
            'additionalData' => $this->getAllAdditionalData(),
        ];
    }

    /**
     * @Route("/create")
     * @Method({"POST"})
     * @Template("AppTemplateBundle:Template:createForm.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed('', TemplateFormType::class);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->getTemplateManager()->saveTemplate($form->getData());
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_template.template.message.created.success'));

            return new RedirectResponse($this->generateUrl('app_template_template_list'));
        }

        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_template.template.message.created.error'));

        return [
            'form' => $form
        ];
    }

    /**
     * @Route("/edit/{template}")
     * @Method({"GET"})
     * @Template("AppTemplateBundle:Template:editForm.html.twig")
     *
     * @param MailTemplate $template
     *
     * @return array
     */
    public function editFormAction(MailTemplate $template)
    {
        $form = $this->get('form.factory')->createNamed('', TemplateFormType::class, $template);

        $additionalData = $this->getAdditionalDataManager()->findAll();

        return [
            'form' => $form,
            'additionalData' => $this->getAllAdditionalData(),
        ];
    }

    /**
     * @Route("/edit/{template}")
     * @Method({"POST"})
     * @Template("AppTemplateBundle:Template:editForm.html.twig")
     *
     * @param Request      $request
     * @param MailTemplate $template
     *
     * @return array
     */
    public function updateAction(Request $request, MailTemplate $template)
    {
        $form = $this->get('form.factory')->createNamed('', TemplateFormType::class, $template);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->getTemplateManager()->saveTemplate($form->getData());
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_template.template.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_template_template_list'));
        }

        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_template.template.message.updated.error'));

        return [
            'form' => $form
        ];
    }

    /**
     * @Route("/delete/{template}")
     *
     * @param MailTemplate $template
     *
     * @return RedirectResponse
     */
    public function deleteAction(MailTemplate $template)
    {
        $this->getTemplateManager()->removeTemplate($template);

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_template.template.message.deleted.success'));

        return new RedirectResponse($this->generateUrl('app_template_template_list'));
    }

    /**
     * @return array
     */
    protected function getAllAdditionalData()
    {
        $emailAdditionalData = new AdditionalData();
        $emailAdditionalData->setName('email');
        $emailAdditionalData->setTag('$$email$$');

        $additionalData = array_merge([$emailAdditionalData], $this->getAdditionalDataManager()->findAll());

        return SerializerBuilder::create()->build()->serialize($additionalData,'json',$this->getSerializationContext());
    }

    /**
     * @return AdditionalDataManager
     */
    protected function getAdditionalDataManager()
    {
        return $this->get('app_recipient.additional_data.manager');
    }

    /**
     * @return TemplateManager
     */
    protected function getTemplateManager()
    {
        return $this->get('app_template.template.manager');
    }

    /**
     * @return SerializationContext
     */
    protected function getSerializationContext()
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $context->setGroups(array('api'));

        return $context;
    }
}