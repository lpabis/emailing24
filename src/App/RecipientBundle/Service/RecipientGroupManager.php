<?php

namespace App\RecipientBundle\Service;

use App\CampaignBundle\Model\CampaignStatusInterface;
use Doctrine\ORM\EntityManager;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Model\RecipientGroupInterface;
use App\RecipientBundle\Repository\RecipientGroupRepository;
use App\RecipientBundle\Exception\GroupCampaignConstraintException;

class RecipientGroupManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var RecipientGroupRepository
     */
    protected $recipientGroupRepository;

    /**
     * @param EntityManager            $entityManager
     * @param RecipientGroupRepository $recipientGroupRepository
     */
    public function __construct(EntityManager $entityManager,
                                RecipientGroupRepository $recipientGroupRepository)
    {
        $this->entityManager = $entityManager;
        $this->recipientGroupRepository = $recipientGroupRepository;
    }

    /**
     * @return RecipientGroup[]|array
     */
    public function findAll()
    {
        return $this->recipientGroupRepository->findAll();
    }

    /**
     * @param array $batchIds
     *
     * @return RecipientGroup[]|array
     */
    public function findByBatchIds(array $batchIds)
    {
        return $this->recipientGroupRepository->findById($batchIds);
    }

    /**
     * @param RecipientGroup $group
     *
     * @return int
     */
    public function calculateRecipientsCounter(RecipientGroup $group)
    {
        $this->entityManager->refresh($group) ;
        $counter = count($group->getRecipients());
        $group->setRecipientCount($counter);
        $this->saveRecipientGroup($group);

        return $counter;
    }

    /**
     * @param RecipientGroup          $recipientGroup
     * @param RecipientGroupInterface $data
     *
     * @return RecipientGroup
     */
    public function updateRecipientGroup(RecipientGroup $recipientGroup, RecipientGroupInterface $data)
    {
        $recipientGroup->setName($data->getName());
        $recipientGroup->setDescription($data->getDescription());

        return $recipientGroup;
    }

    /**
     * @param RecipientGroup $recipientGroup
     * @param bool           $allReferencedEntities
     */
    public function saveRecipientGroup(RecipientGroup $recipientGroup)
    {
        $this->entityManager->persist($recipientGroup);
        $this->entityManager->flush();
    }

    /**
     * @param RecipientGroup $recipientGroup
     */
    public function removeRecipientGroup(RecipientGroup $recipientGroup)
    {
        foreach($recipientGroup->getCampaings() as $campaign) {
            if($campaign->isActive()) {
                throw new GroupCampaignConstraintException('Nie można usunąć grupy. Grupa powiązana z aktywną kampanią.');
            }
        }

        $this->entityManager->remove($recipientGroup);
        $this->entityManager->flush();
    }
}