<?php

namespace App\RecipientBundle\Service;

use App\RecipientBundle\AppRecipientEvents;
use App\RecipientBundle\Event\RecipientEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\RecipientBundle\Entity\Recipient;
use App\RecipientBundle\Repository\RecipientRepository;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Event\RecipientGroupEvent;

class RecipientManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var RecipientRepository
     */
    protected $recipientRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param EntityManager            $entityManager
     * @param RecipientRepository      $recipientRepository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $entityManager,
                                RecipientRepository $recipientRepository,
                                EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->recipientRepository = $recipientRepository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param RecipientGroup $group
     *
     * @return Recipient[]|array
     */
    public function findAllByGroup(RecipientGroup $group)
    {
        return $this->recipientRepository->findBy(['recipientGroup' => $group]);
    }

    /**
     * @param string $email
     *
     * @return null|Recipient
     */
    public function findByEmail($email)
    {
        return $this->recipientRepository->findOneBy(['email' => $email]);
    }

    /**
     * @param Recipient $recipient
     */
    public function removeRecipient(Recipient $recipient)
    {
        $this->entityManager->remove($recipient);
        $this->entityManager->flush();

        $event = new RecipientEvent($recipient);
        $this->dispatcher->dispatch(AppRecipientEvents::RECIPIENT_POST_DELETE, $event);
    }

    /**
     * @param Recipient $recipient
     * @param bool      $allReferencedEntities
     */
    public function saveRecipient(Recipient $recipient)
    {
        $isCreated = null === $recipient->getId();

        if($isCreated && $this->findByEmail($recipient->getEmail())){
            return;
        }

        $this->entityManager->persist($recipient);
        $this->entityManager->flush();

        $event = new RecipientEvent($recipient);

        if($isCreated) {
            $this->dispatcher->dispatch(AppRecipientEvents::RECIPIENT_POST_CREATE, $event);
        }
    }
}