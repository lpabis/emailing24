<?php

namespace App\RecipientBundle\Service;

use App\RecipientBundle\Entity\Recipient;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Service\RecipientManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class TextImporterManager
{
    /**
     * @var RecipientManager
     */
    protected $recipientManager;

    /**
     * @param RecipientManager $recipientManager
     */
    public function __construct(RecipientManager $recipientManager)
    {
        $this->recipientManager = $recipientManager;
    }

    /**
     * @param RecipientGroup $group
     * @param string         $data
     */
    public function import(RecipientGroup $group, $data)
    {
        $dataRows = explode(";", $data);
        foreach($dataRows as $row) {

            $cell = explode(',', $row);

            if(!isset($cell[0])) {
                continue;
            }

            $recipient = new Recipient();
            $recipient->setRecipientGroup($group);
            $recipient->setEmail($cell[0]);

            $this->recipientManager->saveRecipient($recipient);
        };
    }
}