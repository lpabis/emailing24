<?php

namespace App\RecipientBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Repository\AdditionalDataRepository;
use App\RecipientBundle\Entity\AdditionalData;

class AdditionalDataManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var AdditionalDataRepository
     */
    protected $additionalDataRepository;

    /**
     * @param EntityManager            $entityManager
     * @param AdditionalDataRepository $additionalDataRepository
     */
    public function __construct(EntityManager $entityManager,
                                AdditionalDataRepository $additionalDataRepository)
    {
        $this->entityManager = $entityManager;
        $this->additionalDataRepository = $additionalDataRepository;
    }

    /**
     * @return AdditionalData[]|array
     */
    public function findAll()
    {
        return $this->additionalDataRepository->findAllWithoutDuplicates();
    }

    /**
     * @param RecipientGroup $group
     *
     * @return AdditionalData[]|array
     */
    public function findAllByGroup(RecipientGroup $group)
    {
        return $this->additionalDataRepository->findBy(['recipientGroup' => $group]);
    }

    /**
     * @param int $id
     *
     * @return AdditionalData
     */
    public function find($id)
    {
        return $this->additionalDataRepository->find($id);
    }

    /**
     * @return AdditionalData
     */
    public function createEmptyAdditionalData()
    {
        return new AdditionalData();
    }

    /**
     * @param AdditionalData $originalAdditionalData
     * @param AdditionalData $newOriginalAdditionalData
     *
     * @return AdditionalData
     */
    public function updateAdditionalData(AdditionalData $originalAdditionalData, AdditionalData $newOriginalAdditionalData)
    {
        $originalAdditionalData->setName($newOriginalAdditionalData->getName());
        $originalAdditionalData->setTag($newOriginalAdditionalData->getTag());

        return $originalAdditionalData;
    }

    /**
     * @param RecipientGroup  $originalGroup
     * @param ArrayCollection $newFields
     *
     * @return AdditionalData[]|array
     */
    public function updateAdditionalDataCollection(RecipientGroup $originalGroup, $newFields)
    {
        $updatedFields = new ArrayCollection();
        $originalFields = $originalGroup->getAdditionalData();

        foreach($newFields as $newField) {
            if (null === $newField->getId()) {
                $updatedFields->add($newField);
                continue;
            }
            foreach($originalFields as $originalField) {
                if ($newField->getId() === $originalField->getId()) {
                    $this->updateAdditionalData($originalField, $newField);
                    $updatedFields->add($originalField);
                }
            }
        }

        foreach($originalFields as $originalField) {
            if(!$updatedFields->contains($originalField)) {
                $this->removeAdditionalData($originalField);
            }
        }

        $originalGroup->setAdditionalData($updatedFields);

        return $updatedFields;
    }

    /**
     * @param AdditionalData $additionalData
     */
    public function removeAdditionalData(AdditionalData $additionalData)
    {
        $this->entityManager->remove($additionalData);
        $this->entityManager->flush();
    }
}