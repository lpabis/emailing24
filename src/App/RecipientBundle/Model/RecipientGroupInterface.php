<?php

namespace App\RecipientBundle\Model;

interface RecipientGroupInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getDescription();
}