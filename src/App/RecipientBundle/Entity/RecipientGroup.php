<?php

namespace App\RecipientBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use App\RecipientBundle\Model\RecipientGroupInterface;
use App\CampaignBundle\Entity\Campaign;

/**
 * RecipientGroup.
 *
 * @ORM\Table(name="recipient_group")
 * @ORM\Entity(repositoryClass="App\RecipientBundle\Repository\RecipientGroupRepository")
 */
class RecipientGroup implements RecipientGroupInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"api"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank
     * @Serializer\Groups({"api"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Serializer\Groups({"api"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=20, nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $created;

    /**
     * @var int
     *
     * @ORM\Column(name="recipient_count", type="integer", length=11, options={"unsigned":true})
     * @Serializer\Groups({"api"})
     */
    private $recipientCount = 0;

    /**
     * @var Recipient[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Recipient", mappedBy="recipientGroup", cascade={"remove"})
     */
    private $recipients;

    /**
     * @var AdditionalData[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AdditionalData", mappedBy="recipientGroup", cascade={"persist","remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $additionalData;

    /**
     * @var Campaign[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\CampaignBundle\Entity\Campaign", mappedBy="recipientGroups")
     */
    private $campaings;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->hash = substr(md5(mt_rand()), 0, 20);
        $this->recipients = new ArrayCollection();
        $this->additionalData = new ArrayCollection();
        $this->campaings = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getRecipientCount()
    {
        return $this->recipientCount;
    }

    /**
     * @param int $recipientCount
     */
    public function setRecipientCount($recipientCount)
    {
        $this->recipientCount = $recipientCount;
    }

    /**
     * @return Recipient[]|ArrayCollection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param  Recipient[]|ArrayCollection $recipients
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    }

    /**
     * @return AdditionalData[]|ArrayCollection
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * @param AdditionalData[]|ArrayCollection $additionalData
     */
    public function setAdditionalData($additionalData)
    {
        foreach($additionalData as $item) {
            $item->setRecipientGroup($this);
        }
        $this->additionalData = $additionalData;
    }

    /**
     * @return Campaign[]|ArrayCollection
     */
    public function getCampaings()
    {
        return $this->campaings;
    }

    /**
     * @param Campaign[]|ArrayCollection $campaings
     */
    public function setCampaings($campaings)
    {
        $this->campaings = $campaings;
    }
}