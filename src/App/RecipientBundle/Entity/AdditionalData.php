<?php

namespace App\RecipientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * AdditionalData.
 *
 * @ORM\Table(name="additional_data")
 * @ORM\Entity(repositoryClass="App\RecipientBundle\Repository\AdditionalDataRepository")
 */
class AdditionalData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var RecipientGroup
     *
     * @ORM\ManyToOne(targetEntity="RecipientGroup", inversedBy="additionalData")
     * @ORM\JoinColumn(name="recipient_group_id", referencedColumnName="id")
     */
    private $recipientGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank
     * @Serializer\Groups({"api"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=100, nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=20, nullable=false)
     */
    private $hash;

    /**
     * @var RecipientAdditionalData[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RecipientAdditionalData", mappedBy="additionalData", cascade={"remove"})
     */
    private $additionalDataValues;

    public function __construct()
    {
        $this->hash = substr(md5(mt_rand()), 0, 20);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return RecipientGroup
     */
    public function getRecipientGroup()
    {
        return $this->recipientGroup;
    }

    /**
     * @param RecipientGroup $recipientGroup
     */
    public function setRecipientGroup(RecipientGroup $recipientGroup)
    {
        $this->recipientGroup = $recipientGroup;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }
}