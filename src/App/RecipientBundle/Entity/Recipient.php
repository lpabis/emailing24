<?php

namespace App\RecipientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use App\CampaignBundle\Entity\Campaign;
use App\MailQueueBundle\Entity\MailQueue;

/**
 * @ORM\Table(name="recipient")
 * @ORM\Entity(repositoryClass="App\RecipientBundle\Repository\RecipientRepository")
 */
class Recipient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"api"})
     */
    private $id;

    /**
     * @var RecipientGroup
     *
     * @ORM\ManyToOne(targetEntity="RecipientGroup", inversedBy="recipients")
     * @ORM\JoinColumn(name="recipient_group_id", referencedColumnName="id")
     */
    private $recipientGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     * @Assert\NotBlank
     * @Serializer\Groups({"api"})
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $created;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"unsigned":true})
     * @Serializer\Groups({"api"})
     */
    private $status;

    /**
     * @var RecipientAdditionalData[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RecipientAdditionalData", mappedBy="recipient", cascade={"persist","remove"})
     * @Serializer\Groups({"api"})
     */
    private $additionalDataValues;

    /**
     * @var MailQueue[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\MailQueueBundle\Entity\MailQueue", mappedBy="recipient", cascade={"remove"})
     */
    private $mailQueues;

    /**
     * @var Campaign[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\CampaignBundle\Entity\Campaign", mappedBy="recipients")
     */
    private $campaigns;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->additionalDataValues = new ArrayCollection();
        $this->campaigns = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return RecipientGroup
     */
    public function getRecipientGroup()
    {
        return $this->recipientGroup;
    }

    /**
     * @param RecipientGroup $recipientGroup
     */
    public function setRecipientGroup(RecipientGroup $recipientGroup)
    {
        $this->recipientGroup = $recipientGroup;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return RecipientAdditionalData[]|ArrayCollection
     */
    public function getAdditionalDataValues()
    {
        return $this->additionalDataValues;
    }

    /**
     * @param RecipientAdditionalData[]|ArrayCollection $additionalDataValues
     */
    public function setAdditionalDataValues($additionalDataValues)
    {
        foreach($additionalDataValues as $item) {
            $item->setRecipient($this);
        }
        $this->additionalDataValues = $additionalDataValues;
    }

    /**
     * @return Campaign[]|ArrayCollection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @param Campaign[]|ArrayCollection $campaigns
     */
    public function setCampaigns($campaigns)
    {
        $this->campaigns = $campaigns;
    }
}