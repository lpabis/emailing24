<?php

namespace App\RecipientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="recipient_additional_data")
 * @ORM\Entity(repositoryClass="App\RecipientBundle\Repository\RecipientAdditionalDataRepository")
 */
class RecipientAdditionalData
{
    /**
     * @var Recipient
     *
     * @ORM\ManyToOne(targetEntity="Recipient", inversedBy="additionalDataValues")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id", nullable=false)
     * @ORM\Id
     *
     */
    private $recipient;

    /**
     * @var AdditionalData
     *
     * @ORM\ManyToOne(targetEntity="AdditionalData")
     * @ORM\JoinColumn(name="additional_data_id", referencedColumnName="id", nullable=false)
     * @ORM\Id
     * @Serializer\Groups({"api"})
     *
     */
    private $additionalData;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=200, nullable=false)
     * @Serializer\Groups({"api"})
     */
    private $value;

    /**
     * @return Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param Recipient $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return AdditionalData
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * @param AdditionalData $additionalData
     */
    public function setAdditionalData($additionalData)
    {
        $this->additionalData = $additionalData;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}