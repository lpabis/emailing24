<?php

namespace App\RecipientBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AdditionalDataRepository extends EntityRepository
{
    public function findAllWithoutDuplicates()
    {
        return $this->createQueryBuilder('ad')
            ->distinct('ad.name')
            ->getQuery()
            ->getResult();
    }
}
