<?php

namespace App\RecipientBundle\Repository;

use Doctrine\ORM\EntityRepository;
use App\RecipientBundle\Entity\RecipientGroup;

class RecipientGroupRepository extends EntityRepository
{
    /**
     * @param string $hash
     *
     * @return RecipientGroup
     */
    public function findByHash($hash) {

        return $this->findOneBy(['hash' => $hash]);
    }
}
