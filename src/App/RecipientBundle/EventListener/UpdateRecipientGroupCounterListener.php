<?php
namespace App\RecipientBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\RecipientBundle\AppRecipientEvents;
use App\RecipientBundle\Service\RecipientGroupManager;
use App\RecipientBundle\Event\RecipientEvent;

class UpdateRecipientGroupCounterListener implements EventSubscriberInterface
{
    /**
     * @var RecipientGroupManager
     */
    protected $recipientGroupManager;

    /**
     * @param RecipientGroupManager $recipientGroupManager
     */
    public function __construct(RecipientGroupManager $recipientGroupManager)
    {
        $this->recipientGroupManager = $recipientGroupManager;
    }

    /**
     * @param RecipientGroupEvent $event
     */
    public function onPostCreateOrDeleteRecipient(RecipientEvent $event)
    {
        $recipientGroup = $event->getRecipient()->getRecipientGroup();
        $this->recipientGroupManager->calculateRecipientsCounter($recipientGroup);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppRecipientEvents::RECIPIENT_POST_CREATE => ['onPostCreateOrDeleteRecipient'],
            AppRecipientEvents::RECIPIENT_POST_DELETE => ['onPostCreateOrDeleteRecipient']
        ];
    }
}
