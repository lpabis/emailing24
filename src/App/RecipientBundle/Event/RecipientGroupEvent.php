<?php

namespace App\RecipientBundle\Event;

use App\RecipientBundle\Entity\RecipientGroup;
use Symfony\Component\EventDispatcher\Event;

class RecipientGroupEvent extends Event
{
    /**
     * @var RecipientGroup
     */
    protected $recipientGroup;

    /**
     * @param RecipientGroup $recipientGroup
     */
    public function __construct(RecipientGroup $recipientGroup)
    {
        $this->recipientGroup = $recipientGroup;
    }

    /**
     * @return RecipientGroup
     */
    public function getRecipientGroup()
    {
        return $this->recipientGroup;
    }
}
