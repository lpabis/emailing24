<?php

namespace App\RecipientBundle\Event;

use App\RecipientBundle\Entity\Recipient;
use Symfony\Component\EventDispatcher\Event;

class RecipientEvent extends Event
{
    /**
     * @var Recipient
     */
    protected $recipient;

    /**
     * @param Recipient $recipient
     */
    public function __construct(Recipient $recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }
}
