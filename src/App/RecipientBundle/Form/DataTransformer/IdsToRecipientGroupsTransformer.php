<?php

namespace App\RecipientBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use App\RecipientBundle\Service\RecipientGroupManager;
use App\RecipientBundle\Entity\RecipientGroup;

class IdsToRecipientGroupsTransformer implements DataTransformerInterface
{
    /**
     * @var RecipientGroupManager
     */
    protected $recipientGroupManager;

    /**
     * @param RecipientGroupManager $recipientGroupManager
     */
    public function __construct(RecipientGroupManager $recipientGroupManager)
    {
        $this->recipientGroupManager = $recipientGroupManager;
    }

    /**
     * @param array $batchIds
     *
     * @return RecipientGroup[]
     */
    public function reversetransform($batchIds)
    {
        if (null === $batchIds) {
            return;
        }

        return $this->recipientGroupManager->findByBatchIds($batchIds);
    }

    /**
     * @param RecipientGroup[] $recipientGroups
     *
     * @return array
     */
    public function transform($recipientGroups)
    {
        if (null === $recipientGroups) {
            return;
        }

        $arr = [];

        foreach ($recipientGroups as $recipientGroup) {
            $arr[] = $recipientGroup->getId();
        }

        return $arr;
    }
}
