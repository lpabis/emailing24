<?php

namespace App\RecipientBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use App\RecipientBundle\Entity\RecipientGroup;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscribeType extends RecipientType
{
    /**
    * @param FormBuilderInterface $builder
    * @param array                $options
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {

            if(!isset($event->getForm()->getConfig()->getOptions()['group'])) {
                return;
            }

            $recipientGroup = $event->getForm()->getConfig()->getOptions()['group'];
            $additionalDataMapper = $this->getAdditionalDataMapper($recipientGroup);

            $post = $event->getData();

            $data = [];

            if(isset($post['email'])) {
                $data['email'] = $post['email'];
            }

            $data['additionalDataValues'] = $this->getInitialAdditionalDataValues($additionalDataMapper);

            if(isset($post['fields'])) {
                foreach($post['fields'] as $name => $value) {
                    $data['additionalDataValues'][$additionalDataMapper[$name]]['value'] = $value;
                }
            }

            $event->setData($data);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('csrf_protection', false);
    }

    /**
     * @param RecipientGroup $recipientGroup
     *
     * @return array
     */
    protected function getAdditionalDataMapper(RecipientGroup $recipientGroup)
    {
        $additionalDataCollection = $recipientGroup->getAdditionalData();
        $additionalDataMapper = [];
        foreach($additionalDataCollection as $index => $additionalData) {
            $additionalDataMapper[$additionalData->getName()] = $index;
        }

        return $additionalDataMapper;
    }

    /**
     * @param array $additionalDataMapper
     *
     * @return array
     */
    protected function getInitialAdditionalDataValues($additionalDataMapper)
    {
        $data = [];

        foreach($additionalDataMapper as $name => $index) {
            $data[$index]['value'] = '';
        }

        return $data;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_recipient_subscribe';
    }
}
