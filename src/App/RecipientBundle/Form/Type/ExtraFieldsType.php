<?php

namespace App\RecipientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\RecipientBundle\Service\AdditionalDataManager;
use App\RecipientBundle\Entity\RecipientAdditionalData;
use App\RecipientBundle\Repository\RecipientAdditionalDataRepository;

class ExtraFieldsType extends AbstractType
{
    /**
     * @var AdditionalDataManager
     */
    protected $additionalDataManager;

    /**
     * @var RecipientAdditionalDataRepository
     */
    protected $recipientAdditionalDataRepository;

    /**
     * @param AdditionalDataManager             $additionalDataManager
     * @param RecipientAdditionalDataRepository $recipientAdditionalDataRepository
     */
    public function __construct(AdditionalDataManager $additionalDataManager,
                                RecipientAdditionalDataRepository $recipientAdditionalDataRepository)
    {
        $this->additionalDataManager = $additionalDataManager;
        $this->recipientAdditionalDataRepository = $recipientAdditionalDataRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(null === $options['group']) {
            return;
        }

        $fields = $this->additionalDataManager->findAllByGroup($options['group']);
        $recipient = $options['recipient'];

        foreach ($fields as $key => $field) {

            $recipientAdditionalData = null;
            if(null !== $recipient) {
                $recipientAdditionalData = $this->recipientAdditionalDataRepository->findOneBy(['recipient' => $recipient, 'additionalData' => $field]);
            }

            if(null === $recipientAdditionalData){
                $recipientAdditionalData = new RecipientAdditionalData();
                $recipientAdditionalData->setAdditionalData($field);
                $recipientAdditionalData->setRecipient($recipient);
            }

            $builder->add($key, RecipientAdditionalDataType::class,[
                'data' => $recipientAdditionalData,
                'additionalData' => $field,
                'label' => false
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'group' => null,
            'recipient' => null
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_recipient_extra_fields';
    }
}
