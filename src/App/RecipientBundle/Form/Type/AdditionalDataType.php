<?php

namespace App\RecipientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\RecipientBundle\Entity\AdditionalData;

class AdditionalDataType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'app_recipient.additional_data.form.name.label',
                'required' => true,
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('tag', TextType::class, [
                'label' => 'app_recipient.additional_data.form.tag.label',
                'required' => true,
                'attr' => [
                    'readonly' => true,
                ]
            ])
            ->add('entity', HiddenType::class, array('property_path' => 'id'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdditionalData::class,
            'csrf_protection' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_recipient_additional_data';
    }
}
