<?php

namespace App\RecipientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\RecipientBundle\Entity\RecipientAdditionalData;
use App\RecipientBundle\Entity\AdditionalData;

class RecipientAdditionalDataType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(null === $options['additionalData']) {
            return;
        }

        $additionalData = $options['additionalData'];

        $builder
            ->add('value', TextType::class, [
                'label' => ucfirst($additionalData->getName()),
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RecipientAdditionalData::class,
            'additionalData' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_recipient_recipient_additional_data';
    }
}
