<?php

namespace App\RecipientBundle;

final class AppRecipientEvents
{
    const RECIPIENT_POST_CREATE = 'app_recipient.recipient.post_create';
    const RECIPIENT_POST_DELETE = 'app_recipient.recipient.post_delete';
}
