<?php

namespace App\RecipientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\RecipientBundle\Entity\RecipientGroup;

class FormCreatorController extends Controller
{
    /**
     * @Route("/group/{group}/form-creator")
     * @Template()
     *
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function formAction(RecipientGroup $group)
    {
        return [
            'group' => $group,
        ];
    }
}