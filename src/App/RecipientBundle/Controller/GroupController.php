<?php

namespace App\RecipientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\RecipientBundle\Form\Type\RecipientGroupType;
use App\RecipientBundle\Service\RecipientGroupManager;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Exception\GroupCampaignConstraintException;

class GroupController extends Controller
{
    /**
     * @Route("/group/list")
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        return [
            'list' => $this->getRecipientGroupManager()->findAll(),
        ];
    }

    /**
     * @Route("/group/create")
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientGroupType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getRecipientGroupManager()->saveRecipientGroup($form->getData());
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.recipient_group.message.created.success'));

            return new RedirectResponse($this->generateUrl('app_recipient_group_list'));
        }

        if($form->isSubmitted()){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.recipient_group.message.created.error'));
        }

        return [
            'form' => $form,
        ];
    }

    /**
     * @Route("/group/edit/{group}")
     * @Template()
     *
     * @param Request        $request
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function editAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientGroupType::class, $group);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getRecipientGroupManager()->saveRecipientGroup($form->getData());
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.recipient_group.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_recipient_group_list'));
        }

        if($form->isSubmitted()){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.recipient_group.message.updated.error'));
        }

        return [
            'form' => $form,
        ];
    }

    /**
     * @Route("/group/delete/{group}")
     *
     * @param RecipientGroup $group
     *
     * @return RedirectResponse
     */
    public function deleteAction(RecipientGroup $group)
    {
        try {

            $this->getRecipientGroupManager()->removeRecipientGroup($group);
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.recipient_group.message.deleted.success'));

        }catch(GroupCampaignConstraintException $e) {
            $this->get('session')->getFlashBag()->add('error', $e->getMessage());
        }catch(\Exception $e) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.recipient_group.message.deleted.error'));
        }

        return new RedirectResponse($this->generateUrl('app_recipient_group_list'));
    }

    /**
     * @return RecipientGroupManager
     */
    protected function getRecipientGroupManager()
    {
        return $this->get('app_recipient.recipient_group.manager');
    }
}