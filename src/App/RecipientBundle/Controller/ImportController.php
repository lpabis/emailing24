<?php

namespace App\RecipientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Service\TextImporterManager;
use App\RecipientBundle\Form\Type\TextImporterType;

class ImportController extends Controller
{
    /**
     * @Route("/group/{group}/import/text")
     * @Template("AppRecipientBundle:Import:text/form.html.twig")
     *
     * @param Request        $request
     * @param RecipientGroup $group
     */
    public function textAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', TextImporterType::class);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->getTextImporterManager()->import($group, $form['text']->getData());

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.importer.message.import.success'));

            return new RedirectResponse($this->generateUrl('app_recipient_recipient_list',['group' => $group->getId()]));
        }

        if($form->isSubmitted()){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.importer.message.import.error'));
        }

        return [
            'form' => $form,
            'group' => $group
        ];
    }

    /**
     * @return TextImporterManager
     */
    protected function getTextImporterManager()
    {
        return $this->get('app_recipient.text_importer.manager');
    }
}