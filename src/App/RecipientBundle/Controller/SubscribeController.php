<?php

namespace App\RecipientBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\RecipientBundle\Service\RecipientManager;
use App\RecipientBundle\Form\Type\SubscribeType;
use App\RecipientBundle\Entity\RecipientGroup;


class SubscribeController extends Controller
{
    /**
     * @Route("/group/{group}/subscribe")
     * @Method({"GET"})
     * @Template("AppRecipientBundle:Subscribe:form.html.twig")
     *
     * @ParamConverter("group", class="AppRecipientBundle:RecipientGroup", options={"repository_method" = "findByHash"})
     *
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function getCreateFormAction(RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', SubscribeType::class, null, [
            'group' => $group
        ]);

        return [
            'form' => $form
        ];
    }

    /**
     * @Route("/group/{group}/subscribe")
     * @Method({"POST"})
     * @Template("AppRecipientBundle:Subscribe:form.html.twig")
     *
     * @ParamConverter("group", class="AppRecipientBundle:RecipientGroup", options={"repository_method" = "findByHash"})
     *
     * @param Request        $request
     * @param RecipientGroup $group
     *
     * @return RedirectResponse|array
     */
    public function postAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', SubscribeType::class, null, [
            'group' => $group
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $recipient = $form->getData();
                $recipient->setRecipientGroup($group);
                $this->getRecipientManager()->saveRecipient($recipient, true);

                return new RedirectResponse($this->generateUrl('app_recipient_subscribe_success', ['group' => $group->getId()]));

            }catch(UniqueConstraintViolationException $e){
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.subscribe.message.created.error.recipient_exists'));
            }catch(\Exception $e){
                echo $e->getMessage();
            }
        }

        return [
            'form' => $form
        ];
    }

    /**
     * @Route("/group/{group}/subscribe/success")
     * @Template("AppRecipientBundle:Subscribe:success.html.twig")
     *
     * @return array
     */
    public function successAction()
    {

        return [];
    }

    /**
     * @return RecipientManager
     */
    protected function getRecipientManager()
    {
        return $this->get('app_recipient.recipient.manager');
    }
}