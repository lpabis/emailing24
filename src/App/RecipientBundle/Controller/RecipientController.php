<?php

namespace App\RecipientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Entity\Recipient;
use App\RecipientBundle\Form\Type\RecipientType;
use App\RecipientBundle\Service\RecipientManager;

class RecipientController extends Controller
{
    /**
     * @Route("/group/{group}/list")
     * @Template()
     *
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function listAction(RecipientGroup $group)
    {
        return [
            'group' => $group,
            'list' => $this->getRecipientManager()->findAllByGroup($group),
        ];
    }

    /**
     * @Route("/group/{group}/create")
     * @Template("AppRecipientBundle:Recipient:create.html.twig")
     *
     * @param Request        $request
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function createAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientType::class, null, [
            'group' => $group
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $recipient = $form->getData();
            $recipient->setRecipientGroup($group);
            $this->getRecipientManager()->saveRecipient($recipient, true);

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.recipient.message.created.success'));

            return new RedirectResponse($this->generateUrl('app_recipient_recipient_list',['group' => $group->getId()]));
        }

        if($form->isSubmitted()){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.recipient.message.created.error'));
        }

        return [
            'form' => $form,
            'group' => $group
        ];
    }

    /**
     * @Route("/group/{group}/edit/{recipient}")
     * @Template("AppRecipientBundle:Recipient:edit.html.twig")
     *
     * @param Request        $request
     * @param RecipientGroup $group
     * @param Recipient      $recipient
     *
     * @return array
     */
    public function editAction(Request $request, RecipientGroup $group, Recipient $recipient)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientType::class, $recipient, [
            'group' => $group
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->getRecipientManager()->saveRecipient($form->getData());

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.recipient.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_recipient_recipient_list',['group' => $group->getId()]));
        }

        if($form->isSubmitted()){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.recipient.message.updated.error'));
        }

        return [
            'form' => $form,
            'group' => $group
        ];
    }

    /**
     * @Route("/group/{group}/delete/{recipient}")
     *
     * @param Request        $request
     * @param RecipientGroup $group
     * @param Recipient      $recipient
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, RecipientGroup $group, Recipient $recipient)
    {
        $this->getRecipientManager()->removeRecipient($recipient);

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.recipient.message.deleted.success'));

        return new RedirectResponse($this->generateUrl('app_recipient_recipient_list',['group' => $group->getId()]));
    }

    /**
     * @return RecipientManager
     */
    protected function getRecipientManager()
    {
        return $this->get('app_recipient.recipient.manager');
    }
}