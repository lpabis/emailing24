<?php

namespace App\RecipientBundle\Controller;

use App\RecipientBundle\Entity\RecipientGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RaportController extends Controller
{
    /**
     * @Route("/{group}/raport")
     *
     * @param Request        $request
     * @param RecipientGroup $group
     */
    public function getAction(Request $request, RecipientGroup $group)
    {
        return $this->render('AppRecipientBundle:Raport:get.html.twig');
    }
}