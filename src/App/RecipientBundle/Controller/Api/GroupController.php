<?php

namespace App\RecipientBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\Secure;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use App\RecipientBundle\Form\Type\RecipientGroupType;
use App\RecipientBundle\Service\RecipientGroupManager;
use App\RecipientBundle\Entity\RecipientGroup;

/**
 * @RouteResource("/api/recipient/group", pluralize=false)
 */
class GroupController extends FOSRestController
{
    /**
     * @ApiDoc(
     *   section = "Grupy odbiorców",
     *   output = {
     *     "class" = "App\RecipientBundle\Entity\RecipientGroup",
     *     "groups" = {"api"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @return View
     */
    public function getListAction()
    {
        return View::create()
            ->setStatusCode(200)
            ->setContext($this->getSerializationContext())
            ->setData([
                'list' => $this->getRecipientGroupManager()->findAll(),
            ]);
    }

     /**
      * @ApiDoc(
      *   section = "Grupy odbiorców",
      *   input = {
      *     "class" = "App\RecipientBundle\Form\Type\RecipientGroupType",
      *     "name" = "",
      *   },
      *   output = {
      *     "class" = "App\RecipientBundle\Entity\RecipientGroup",
      *     "groups" = {"api"},
      *   },
      *   statusCodes = {
      *     201 = "Returned when successful",
      *     400 = "Returned when the form has errors"
      *   }
      * )
      *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientGroupType::class, null, [
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        $viewData = $form;

        if ($form->isValid()) {
            $group = $form->getData();
            $this->getRecipientGroupManager()->saveRecipientGroup($group);
            $viewData = $group;

            return View::create()
                ->setStatusCode(201)
                ->setContext($this->getSerializationContext())
                ->setData($viewData);

        }

        return View::create()
            ->setStatusCode(400)
            ->setContext($this->getSerializationContext())
            ->setData($viewData);
    }

    /**
     * @ApiDoc(
     *   section = "Grupy odbiorców",
     *   input = {
     *     "class" = "App\RecipientBundle\Form\Type\RecipientGroupType",
     *     "name" = "",
     *   },
     *   output = {
     *     "class" = "App\RecipientBundle\Entity\RecipientGroup",
     *     "groups" = {"api"},
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request        $request
     * @param RecipientGroup $group
     *
     * @return View
     */
    public function patchAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientGroupType::class, $group, [
            'csrf_protection' => false,
            'method' => 'PATCH',
        ]);

        $form->handleRequest($request);

        $viewData = $form;

        if ($form->isValid()) {
            $this->getRecipientGroupManager()->saveRecipientGroup($group);
            $viewData = $group;

            return View::create()
                ->setStatusCode(200)
                ->setContext($this->getSerializationContext())
                ->setData($viewData);
        }

        return View::create()
            ->setStatusCode(400)
            ->setContext($this->getSerializationContext())
            ->setData($viewData);
    }

    /**
     * @ApiDoc(
     *   section = "Grupy odbiorców",
     *   statusCodes={
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param RecipientGroup $group
     *
     * @return View
     */
    public function deleteAction(RecipientGroup $group)
    {
        $this->getRecipientGroupManager()->removeRecipientGroup($group);

        return View::create()
            ->setStatusCode(204);
    }

    /**
     * @return RecipientGroupManager
     */
    protected function getRecipientGroupManager()
    {
        return $this->get('app_recipient.recipient_group.manager');
    }

    /**
     * @return Context
     */
    protected function getSerializationContext()
    {
        $context = new Context();
        $context->setSerializeNull(true);
        $context->setGroups(array('api'));

        return $context;
    }
}