<?php

namespace App\RecipientBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use App\RecipientBundle\Entity\RecipientGroup;
use App\RecipientBundle\Form\Type\RecipientType;
use App\RecipientBundle\Service\RecipientManager;
use App\RecipientBundle\Entity\Recipient;

/**
 * @RouteResource("/api/recipient/recipient", pluralize=false)
 */
class RecipientController extends FOSRestController
{
    /**
     * @ApiDoc(
     *   section = "Odbiorcy",
     *   output = {
     *     "class" = "App\RecipientBundle\Entity\Recipient",
     *     "groups" = {"api"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @param RecipientGroup $group
     *
     * @return View
     */
    public function getListAction(RecipientGroup $group)
    {
        return View::create()
            ->setStatusCode(200)
            ->setContext($this->getSerializationContext())
            ->setData([
                'list' => $this->getRecipientManager()->findAllByGroup($group),
            ]);
    }

    /**
     * @ApiDoc(
     *   section = "Odbiorcy",
     *   input = {
     *     "class" = "App\RecipientBundle\Form\Type\RecipientType",
     *     "name" = "",
     *   },
     *   output = {
     *     "class" = "App\RecipientBundle\Entity\Recipient",
     *     "groups" = {"api"},
     *   },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request        $request
     * @param RecipientGroup $group
     *
     * @return View
     */
    public function postAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', RecipientType::class, null, [
            'group' => $group
        ]);

        $form->handleRequest($request);

        $viewData = $form;

        if ($form->isValid()) {

            $recipient = $form->getData();
            $recipient->setRecipientGroup($group);
            $this->getRecipientManager()->saveRecipient($recipient, true);
            $viewData = $recipient;

            return View::create()
                ->setStatusCode(201)
                ->setContext($this->getSerializationContext())
                ->setData($viewData);
        }

        return View::create()
            ->setStatusCode(400)
            ->setContext($this->getSerializationContext())
            ->setData($viewData);
    }

    /**
     * @ApiDoc(
     *   section = "Odbiorcy",
     *   statusCodes={
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Recipient $recipient
     *
     * @return View
     */
    public function deleteAction(Recipient $recipient)
    {
        $this->getRecipientManager()->removeRecipient($recipient);

        return View::create()
            ->setStatusCode(204);
    }

    /**
     * @return RecipientManager
     */
    protected function getRecipientManager()
    {
        return $this->get('app_recipient.recipient.manager');
    }

    /**
     * @return Context
     */
    protected function getSerializationContext()
    {
        $context = new Context();
        $context->setSerializeNull(true);
        $context->setGroups(array('api'));

        return $context;
    }
}