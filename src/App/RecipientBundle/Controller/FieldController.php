<?php

namespace App\RecipientBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\RecipientBundle\Form\Type\AdditionalDataCollectionType;
use App\RecipientBundle\Service\RecipientGroupManager;
use App\RecipientBundle\Service\AdditionalDataManager;
use App\RecipientBundle\Entity\RecipientGroup;

class FieldController extends Controller
{
    /**
     * @Route("/group/{group}/fields")
     * @Template()
     *
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function listAction(RecipientGroup $group)
    {
        return [
            'list' => $this->getAdditionalDataManager()->findAllByGroup($group),
        ];
    }

    /**
     * @Route("/group/{group}/fields/edit")
     * @Method({"GET"})
     * @Template("AppRecipientBundle:Field:form.html.twig")
     *
     * @param RecipientGroup $group
     *
     * @return array
     */
    public function editFormAction(RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', AdditionalDataCollectionType::class, ['fields' => $group->getAdditionalData()]);

        return [
            'form' => $form,
            'group' => $group,
        ];
    }

    /**
     * @Route("/group/{group}/fields/edit")
     * @Method({"POST"})
     * @Template("AppRecipientBundle:Field:form.html.twig")
     *
     * @param Request        $request
     * @param RecipientGroup $group
     *
     * @return array|RedirectResponse
     */
    public function updateAction(Request $request, RecipientGroup $group)
    {
        $form = $this->get('form.factory')->createNamed('', AdditionalDataCollectionType::class, ['fields' => $group->getAdditionalData()]);

        if (!$request->request->has('fields')) {
            $request->request->set('fields', []);
        }

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->getAdditionalDataManager()->updateAdditionalDataCollection($group, $form['fields']->getData());
            $this->getRecipientGroupManager()->saveRecipientGroup($group, true);

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app_recipient.additional_data.message.updated.success'));

            return new RedirectResponse($this->generateUrl('app_recipient_field_editform', ['group' => $group->getId()]));
        }

        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app_recipient.additional_data.message.updated.error'));

        return [
            'form' => $form
        ];
    }

    /**
     * @return RecipientGroupManager
     */
    protected function getRecipientGroupManager()
    {
        return $this->get('app_recipient.recipient_group.manager');
    }

    /**
     * @return AdditionalDataManager
     */
    protected function getAdditionalDataManager()
    {
        return $this->get('app_recipient.additional_data.manager');
    }
}