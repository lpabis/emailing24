$(function() {

    function addTagForm($collectionHolder) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.find('tr').length-2;

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.attr('data-index', index+1);

        // Display the form in the page in an li, before the "Add a tag" link li
        $collectionHolder.append(newForm);

        $collectionHolder.find("tr:last .remove-fields").click(function(){
            removeTagForm($collectionHolder, $(this).parent().parent("tr"));
        })

        $collectionHolder.find("tr:last td.field-name-td input").keyup(function(){
            gemerateTagElement($(this), $(this).parent().parent().parent("tr").find("td.field-tag-td input"));
        })
    }

    function removeTagForm($collectionHolder, $element) {
        $element.fadeOut(function() {
            $(this).remove();
        });
    }

    function gemerateTagElement($nameElement, $tagElement) {
        $tagElement.val("$$"+$nameElement.val().escapeDiacritics()+"$$");
    }

    var $collectionHolder = $('#fields-table');

    // setup an "add a tag" link
    var $addTagLink = $('#add-field');

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)

    $collectionHolder.attr('data-index', $collectionHolder.find('tr').length-2);

    $addTagLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        // add a new tag form (see next code block)
        addTagForm($('#fields-table'));
    });

    $(".remove-fields").click(function(){
        removeTagForm($collectionHolder, $(this).parent().parent("tr"));
    })

    $("td.field-name-td input").keyup(function(){
        gemerateTagElement($(this), $(this).parent().parent().parent("tr").find("td.field-tag-td input"));
    })

});
