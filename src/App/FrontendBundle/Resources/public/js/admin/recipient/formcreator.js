var FormCreator = {

    settings: {},

    initSettings: function(url, group) {
        this.settings.url = url;
        this.settings.group = group;
    },

    generateForm: function(fields) {

        var form = '<form method="post" action="'+this.settings.url+'">';

        fields.forEach(function(field) {
            form += '<div class="form-group"><label for="emailing_'+field.name+'">'+field.title+'</label><input type="text" id="emailing_'+field.name+'" name="'+(field.name == 'email' ? 'email' : 'fields['+field.name+']')+'"/></div>';
        });

        form += '<div><input type="submit" value="Wyślij" /></div>';
        form += '</form>';

        return form;
    },

    /*return {
        init: function () {
            initSettings({
                'url': url,
                'group': group
            });
        }
    }*/
}



$(function() {

    $("#form-generate-btn").click(function(){

        var fields = new Array();

        $(".field-checkbox").each(function(index, checkbox) {
            if($(checkbox).is(':checked')) {
                fields.push({title: $('#'+$(checkbox).val()+'-title').val(), name: $(checkbox).val()});
            }
        })

        $("#form-generate-output").html(FormCreator.generateForm(fields));
    })
})