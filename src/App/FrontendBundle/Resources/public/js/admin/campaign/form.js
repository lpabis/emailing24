$(function() {

    $("input.js-switch").change(function(){
        if($(this).is(":checked")){
            $("#isABTestEnabledContainer").removeClass("form_disabled");
            $("#isABTestEnabledContainer input").attr('disabled', false);
        }else{
            $("#isABTestEnabledContainer").addClass("form_disabled");
            $("#isABTestEnabledContainer input").attr('disabled', true);
        }
    })

    $("input[name='abTest[percentage]']").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });

    $("input[name='abTest[finishAfterSeconds]']").TouchSpin({
        min: 0,
        max: 100000,
        step: 10,
        decimals: 0,
        postfix: 'sekund'
    });

    $('#start-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });

    $('#start-timepicker').timepicker({
        showMeridian: false,
    });

    $('#end-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });

    $('#end-timepicker').timepicker({
        showMeridian: false,
    });

    $("#sendnow-radio1").change(function(){
        if($(this).is(":checked")){
            $("#dateFormContainer").addClass("form_disabled");
            $("#dateFormContainer input").attr('disabled', true);
        }else{
            $("#dateFormContainer").removeClass("form_disabled");
            $("#dateFormContainer input").attr('disabled', false);
        }
    })

    $("#sendnow-radio2").change(function(){
        if($(this).is(":checked")){
            $("#dateFormContainer").removeClass("form_disabled");
            $("#dateFormContainer input").attr('disabled', false);
        }else{
            $("#dateFormContainer").addClass("form_disabled");
            $("#dateFormContainer input").attr('disabled', true);
        }
    })

    $("#sendnow-radio1").change();
});
