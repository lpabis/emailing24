var TextEditor = function() {"use strict";
	//function to initiate ckeditor
	var ckEditorHandler = function($tags) {
		CKEDITOR.disableAutoInline = true;
		CKEDITOR.config.height = 400;
		CKEDITOR.plugins.add( 'addEmailingTag', {
			icons: false,
			init: function( editor ) {
				// Plugin logic goes here...
			}
		});

		CKEDITOR.dialog.add( 'addEmailingTagDialog', function( editor ) {
			return {
				title: 'Wstaw swój tag',
				minWidth: 400,
				minHeight: 100,

				contents: [{
					id: 'default-content',
					elements: [
						{
							type: 'select',
							id: 'tag',
							items: [
								['Wybierz', '']
							],
							onLoad: function(element) {
								var selectbox = this;
								$tags.forEach(function(tag) {
									selectbox.add(tag.name, tag.tag);
								})
							}
						}
					]
				}],

				buttons: [

					CKEDITOR.dialog.cancelButton,

					{
						type: 'button',
						label: 'Wstaw',
						title: 'Wstaw',
						accessKey: 'C',
						disabled: false,
						onClick: function()
						{
							var dialog = CKEDITOR.dialog.getCurrent();

							var tag = dialog.getValueOf('default-content','tag');

							editor.insertText(tag);

							dialog.hide();

							/*var abbr = editor.document.createElement( 'abbr' );
							 abbr.setAttribute( 'title', dialog.getValueOf( 'tab-basic', 'title' ) );
							 abbr.setText( dialog.getValueOf( 'tab-basic', 'abbr' ) );

							 var id = dialog.getValueOf( 'tab-adv', 'id' );
							 if ( id )
							 abbr.setAttribute( 'id', id );*/

							//editor.insertElement( abbr );
						},
					},

				],
			};
		});

		var ckeditor = $('textarea.ckeditor').ckeditor();

		ckeditor.editor.addCommand( 'addEmailingTag', new CKEDITOR.dialogCommand( 'addEmailingTagDialog' ) );

		ckeditor.editor.ui.addButton( 'AddEmailingTag', {
			label: 'Wstaw swój tag',
			command: 'addEmailingTag',
			toolbar: 'emailing'
		});

		/*$('textarea.ckeditor').ckeditor(
			{
				height: 400,
			});*/
	};

	return {
		//main function to initiate template pages
		init: function($tags) {
			ckEditorHandler($tags);
		}
	};
}();

$(function() {

	/*$("#insert-tag-btn").click(function(){
		$('textarea.ckeditor').ckeditor().editor.insertText('some text here');
	})*/
})
