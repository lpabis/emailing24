<?php

namespace App\FrontendBundle\Service;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @param FactoryInterface         $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return ItemInterface
     */
    public function createMainMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        $menu = $this->factory->createItem('root');

        /*PULPIT*/
        $menu->addChild('dashboard', ['label' => 'Pulpit', 'route' => 'app_homepage_dashboard_index'])->setAttribute('icon', 'ti-home');

        /*ODBIORCY*/
        $menu->addChild('recipients', ['label' => 'Grupa odbiorców'])->setCurrent(in_array($request->get('_route'), [
            'app_recipient_group_list',
            'app_recipient_group_create',
            'app_recipient_group_edit',
            'app_recipient_recipient_list',
            'app_recipient_formcreator_form',
            'app_recipient_field_editform',
            'app_recipient_field_update',
            'app_recipient_recipient_create',
            'app_recipient_recipient_edit',
            'app_recipient_import_text',
        ]))->setAttribute('icon', 'ti-user');
        $menu['recipients']->addChild('list', ['label' => 'Lista','route' => 'app_recipient_group_list']);
        $menu['recipients']->addChild('create', ['label' => 'Utwórz','route' => 'app_recipient_group_create']);

        /*SZABLONY*/
        $menu->addChild('templates', ['label' => 'Szablony'])->setCurrent(in_array($request->get('_route'), [
            'app_template_template_list',
            'app_template_template_getcreateform',
            'app_template_template_editform',
            'app_template_template_get',
        ]))->setAttribute('icon', 'ti-layers-alt');
        $menu['templates']->addChild('list', ['label' => 'Lista','route' => 'app_template_template_list']);
        $menu['templates']->addChild('create', ['label' => 'Utwórz','route' => 'app_template_template_getcreateform']);

        /*KAMPANIE*/
        $menu->addChild('campaigns', ['label' => 'Kampanie'])->setCurrent(in_array($request->get('_route'), [
            'app_campaign_mailing_list',
            'app_campaign_mailing_createform',
            'app_campaign_mailing_editform',
            'app_campaign_mailing_get',
            'app_campaign_autorespondercyclic_list',
            'app_campaign_autorespondercyclic_createform',
            'app_campaign_autorespondercyclic_editform',
            'app_campaign_autorespondercyclic_get',
            'app_campaign_autorespondersignup_list',
            'app_campaign_autorespondersignup_createform',
            'app_campaign_autorespondersignup_editform',
            'app_campaign_autorespondersignup_get',
        ]))->setAttribute('icon', 'ti-package');

        /*MAILING*/
        $menu['campaigns']->addChild('mailings', ['label' => 'Mailing'])->setCurrent(in_array($request->get('_route'), [
            'app_campaign_mailing_list',
            'app_campaign_mailing_createform',
            'app_campaign_mailing_get'
        ]));
        $menu['campaigns']['mailings']->addChild('list', ['label' => 'Lista','route' => 'app_campaign_mailing_list']);
        $menu['campaigns']['mailings']->addChild('create', ['label' => 'Utwórz','route' => 'app_campaign_mailing_createform']);

        /*AUTORESPONDER*/
        $menu['campaigns']->addChild('autoresponders', ['label' => 'Autoresponder'])->setCurrent(in_array($request->get('_route'), [
            'app_campaign_autorespondercyclic_list',
            'app_campaign_autorespondercyclic_createform',
            'app_campaign_autorespondercyclic_editform',
            'app_campaign_autorespondercyclic_get',
            'app_campaign_autorespondersignup_list',
            'app_campaign_autorespondersignup_createform',
            'app_campaign_autorespondersignup_editform',
            'app_campaign_autorespondersignup_get',
        ]));

        $menu['campaigns']['autoresponders']->addChild('cylicAutoresponders', ['label' => 'Cykliczny'])->setCurrent(in_array($request->get('_route'), [
            'app_campaign_autorespondercyclic_list',
            'app_campaign_autorespondercyclic_createform',
            'app_campaign_autorespondercyclic_editform',
            'app_campaign_autorespondercyclic_get',
        ]));
        $menu['campaigns']['autoresponders']['cylicAutoresponders']->addChild('list', ['label' => 'Lista','route' => 'app_campaign_autorespondercyclic_list']);
        $menu['campaigns']['autoresponders']['cylicAutoresponders']->addChild('create', ['label' => 'Utwórz','route' => 'app_campaign_autorespondercyclic_createform']);

        $menu['campaigns']['autoresponders']->addChild('singupAutoresponders', ['label' => 'Zapis na listę odbiorców'])->setCurrent(in_array($request->get('_route'), [
            'app_campaign_autorespondersignup_list',
            'app_campaign_autorespondersignup_createform',
            'app_campaign_autorespondersignup_editform',
            'app_campaign_autorespondersignup_get',
        ]));
        $menu['campaigns']['autoresponders']['singupAutoresponders']->addChild('list', ['label' => 'Lista','route' => 'app_campaign_autorespondersignup_list']);
        $menu['campaigns']['autoresponders']['singupAutoresponders']->addChild('create', ['label' => 'Utwórz','route' => 'app_campaign_autorespondersignup_createform']);

        /*USTAWIENIA*/
        $menu->addChild('settings', ['label' => 'Ustawienia'])->setCurrent(in_array($request->get('_route'), [
            'app_setting_smtp_editform',
            'app_setting_api_editform'
        ]))->setAttribute('icon', 'ti-settings');

        /*USTAWIENIA-SMTP*/
        $menu['settings']->addChild('smtp', ['label' => 'Smtp','route' => 'app_setting_smtp_editform']);

        /*USTAWIENIA-API*/
        $menu['settings']->addChild('api', ['label' => 'Api','route' => 'app_setting_api_editform']);

        return $menu;
    }

    /**
     * @return ItemInterface
     */
    public function createMainSubMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        $menu = $this->factory->createItem('root');

        switch($request->get('_route')) {
            case 'app_recipient_recipient_list':
            case 'app_recipient_formcreator_form':
            case 'app_recipient_field_editform':
            case 'app_recipient_field_update':
            case 'app_recipient_recipient_create':
            case 'app_recipient_recipient_edit':
            case 'app_recipient_import_text':
            case 'app_recipient_group_edit':
                $group = $request->get('group');
                $menu->setLabel($group->getName());
                $menu->addChild('addresses', ['label' => 'Odbiorcy', 'route' => 'app_recipient_recipient_list', 'routeParameters' => ['group' => $group->getId()]])->setAttribute('icon', 'ti-email');
                $menu->addChild('edit', ['label' => 'Edytuj grupę', 'route' => 'app_recipient_group_edit', 'routeParameters' => ['group' => $group->getId()]])->setAttribute('icon', 'ti-pencil');
                $menu->addChild('delete', ['label' => 'Usuń grupę', 'route' => 'app_recipient_group_delete', 'routeParameters' => ['group' => $group->getId()]])->setAttributes(['icon' => 'ti-trash','class' => 'remove-popup']);
                $menu->addChild('sign', ['label' => 'Formularz zapisu do grupy', 'route' => 'app_recipient_formcreator_form', 'routeParameters' => ['group' => $group->getId()]])->setAttribute('icon', 'ti-check-box');
                $menu->addChild('additional_fields', ['label' => 'Pola dodatkowe', 'route' => 'app_recipient_field_editform', 'routeParameters' => ['group' => $group->getId()]])->setAttribute('icon', 'ti-menu');
                break;
            case 'app_template_template_get':
            case 'app_template_template_editform':
                $template = $request->get('template');
                $menu->setLabel($template->getName());
                $menu->addChild('get', ['label' => 'Podgląd szablonu', 'route' => 'app_template_template_get', 'routeParameters' => ['template' => $template->getId()]])->setAttribute('icon', 'ti-eye');
                $menu->addChild('edit', ['label' => 'Edytuj szablon', 'route' => 'app_template_template_editform', 'routeParameters' => ['template' => $template->getId()]])->setAttribute('icon', 'ti-pencil');
                $menu->addChild('delete', ['label' => 'Usuń szablon', 'route' => 'app_template_template_delete', 'routeParameters' => ['template' => $template->getId()]])->setAttributes(['icon' => 'ti-trash','class' => 'remove-popup']);
                break;
        }

        return $menu;
    }
}