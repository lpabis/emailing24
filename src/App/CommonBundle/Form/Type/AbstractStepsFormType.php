<?php

namespace App\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Validator\Constraints\Valid;

abstract class AbstractStepsFormType extends AbstractType
{
    /**
     * @var FormTypeInterface[]
     */
    protected $steps;

    /**
     * @var EventSubscriberInterface[]
     */
    protected $subscribers;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @param Router                     $router
     * @param FormTypeInterface[]        $steps
     * @param EventSubscriberInterface[] $subscribers
     */
    public function __construct(Router $router,
                                array $steps,
                                array $subscribers = array())
    {
        $this->router = $router;
        $this->steps = $steps;
        $this->subscribers = $subscribers;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currentStep = null !== $options['step'] ? $options['step'] : $this->countSteps();
        $prefixName = null !==  $options['prefixName'] ? $options['prefixName'] : '';
        foreach ($this->steps as $key => $subForm) {
            $this->addStep($builder, $subForm, $key, $prefixName);

            if ($this->isLastSubFormToRenderInThisStep($currentStep, $key)) {
                break;
            }
        }

        foreach ($this->subscribers as $subscriber) {
            $builder->addEventSubscriber($subscriber);
        }

        if (null !== $options['routeName']) {
            $backUrl = $this->router->generate($options['routeName'], [
                'step' => $currentStep > 0 ? $currentStep - 1 : 0
            ]);

            $builder->add('back', ButtonType::class, [
                'disabled' => $currentStep == 1,
                'label' => 'back',
                'attr' => [
                    'onClick' => "location.href='{$backUrl}'"]
            ]);
        };

        $builder->add('next', SubmitType::class, [
            'disabled' => $currentStep >= $this->countSteps(),
            'label' => 'next',
        ])->add('save', SubmitType::class, [
            'disabled' => $currentStep < $this->countSteps(),
            'label' => 'submit',
        ]);
    }

    /**
     * @param int $currentStep
     * @param int $keyOfAddedSubForm
     *
     * @return bool
     */
    protected function isLastSubFormToRenderInThisStep($currentStep, $keyOfAddedSubForm)
    {
        return (int) $currentStep === $keyOfAddedSubForm + 1;
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['step'] = [
            'current' => null === $options['step'] ? $this->countSteps() : (int) $options['step'],
            'total' => $this->countSteps(),
        ];

        $view->vars['attr'] = $this->initVarsAttr($view->vars['attr']);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param FormTypeInterface    $subForm
     * @param int                  $step
     */
    protected function addStep(FormBuilderInterface $builder, FormTypeInterface $subForm, $step, $prefixName)
    {
        $translationsPrefix = sprintf('%s%s.form', $prefixName, $subForm->getName());
        $builder->add($subForm->getName(), get_class($subForm), [
            'label' => sprintf('%s.label', $translationsPrefix),
            'attr' => ['data-step' => $step, 'class' => 'step-form'],
            'constraints' => new Valid()
        ]);
    }

    /**
     * @return int
     */
    protected function countSteps()
    {
        return count($this->steps);
    }

    /**
     * @param array $attr
     *
     * @return array
     */
    abstract protected function initVarsAttr(array $attr);
}
