<?php

namespace Tests\AppBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class GroupControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->loadFixtures([]);

        $this->client = static::createClient();
        $this->logIn();
    }

    public function testList()
    {
        $crawler = $this->client->request('GET', '/recipient/group/list');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Lista grup odbiorców', $crawler->filter('#container h1')->text());
    }

    public function testCreate()
    {
        $crawler = $this->client->request('GET', '/recipient/group/create');

        $form = $crawler->filter('button[type="submit"]')->form(null, 'POST');
        $form['name'] = 'Grupa group398763';
        $form['description'] = 'Opis grupy group398763';

        $this->client->submit($form);

        $newGroup = $this->getContainer()->get('app_recipient.recipient_group.repository')->findOneBy(['name' => $form['name']]);

        $this->assertNotNull($newGroup);
        $this->assertEquals($newGroup->getDescription(), $form['description']);
    }

    private function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        $firewallContext = 'default';

        $token = new UsernamePasswordToken('admin', null, $firewallContext, array('ROLE_USER'));
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
